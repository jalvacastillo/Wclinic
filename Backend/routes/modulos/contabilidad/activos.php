<?php 

use App\Http\Controllers\Api\Contabilidad\ActivosController;

    Route::get('/activos',             [ActivosController::class, 'index']);
    Route::post('/activo',             [ActivosController::class, 'store']);
    Route::get('/activo/{id}',         [ActivosController::class, 'read']);
    Route::post('/activos/filtrar',         [ActivosController::class, 'filter']);
    Route::delete('/activo/{id}',         [ActivosController::class, 'delete']);

?>
