<?php 

use App\Http\Controllers\Api\Empleados\EmpleadosController;

use App\Http\Controllers\Api\Empleados\PlanillasController;
use App\Http\Controllers\Api\Empleados\PlanillaDetallesController;
use App\Http\Controllers\Api\Empleados\ContratosController;

use App\Http\Controllers\Api\Empleados\ComisionesController;

// Empleados 
    Route::get('/empleados',                 [EmpleadosController::class, 'index']);
    Route::get('/empleados/list',            [EmpleadosController::class, 'list']);
    Route::get('/empleados/buscar/{text}',   [EmpleadosController::class, 'search']);
    Route::post('/empleados/filtrar',         [EmpleadosController::class, 'filter']);
    Route::post('/empleado',                 [EmpleadosController::class, 'store']);
    Route::get('/empleado/{id}',             [EmpleadosController::class, 'read']);
    Route::delete('/empleado/{id}',          [EmpleadosController::class, 'delete']);

    Route::delete('/empleado/comisiones/{id}',          [EmpleadosController::class, 'comisiones']);



// Planilla
    Route::get('/planillas',                  [PlanillasController::class, 'index']);
    Route::get('/planillas/buscar/{text}',    [PlanillasController::class, 'search']);
    Route::post('/planillas/filtrar',         [PlanillasController::class, 'filter']);
    Route::post('/planilla',                 [PlanillasController::class, 'store']);
    Route::get('/planilla/{id}',             [PlanillasController::class, 'read']);
    Route::delete('/planilla/{id}',          [PlanillasController::class, 'delete']);

    Route::post('/planilla/detalle',                 [PlanillaDetallesController::class, 'store']);
    Route::get('/planilla/detalle/{id}',             [PlanillaDetallesController::class, 'read']);
    Route::delete('/planilla/detalle/{id}',          [PlanillaDetallesController::class, 'delete']);

// Contrato
    Route::post('/contrato',                 [ContratosController::class, 'store']);
    Route::get('/contrato/{id}',             [ContratosController::class, 'read']);
    Route::delete('/contrato/{id}',          [ContratosController::class, 'delete']);

// Comisiones
    Route::get('/comisiones',                [ComisionesController::class, 'index']);
    Route::post('/comision',                 [ComisionesController::class, 'store']);
    Route::post('/comisiones/filtrar',             [ComisionesController::class, 'filter']);
    Route::get('/comision/{id}',             [ComisionesController::class, 'read']);
    Route::delete('/comision/{id}',          [ComisionesController::class, 'delete']);

    
?>
