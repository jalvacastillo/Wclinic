<?php 

use App\Http\Controllers\Api\DashController;

    
    Route::post('/dash',                        [DashController::class, 'index']);
    Route::post('/admin',                       [DashController::class, 'admin']);
    Route::post('/galonajes',                   [DashController::class, 'galonaje']);
    Route::post('/estadistica',                 [DashController::class, 'estadistica']);
    Route::post('/telefonia-datos',             [DashController::class, 'telefoniaDatos']);

    Route::get('/dash/cocinero',                [DashController::class, 'cocinero']);
    Route::get('/dash/cocinero/departamento/{id}',  [DashController::class, 'cocineroDepartamento']);
    Route::get('/dash/mesero',                  [DashController::class, 'mesero']);
    
    Route::get('/dash/vendedor',                [DashController::class, 'vendedor']);

    Route::get('/dash/cajero/{id}',             [DashController::class, 'cajero']);

