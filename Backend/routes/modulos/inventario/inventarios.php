<?php
	
use App\Http\Controllers\Api\Inventario\InventariosController;

    Route::get('/inventario/{id}',                 	[InventariosController::class, 'index']);
    Route::get('/inventario/{id}',                  [InventariosController::class, 'read']);
	Route::post('/inventario/filtrar',              [InventariosController::class, 'filter']);
    Route::post('/inventario',                 		[InventariosController::class, 'store']);
    Route::delete('/inventario/{id}',               [InventariosController::class, 'delete']);
    
    Route::get('/inventario/buscar/{txt}',          [InventariosController::class, 'inventarioSearch']);
    Route::get('/inventario/sala-venta/buscar/{txt}', [InventariosController::class, 'ventaSearch']);