<?php 

use App\Http\Controllers\Api\Pacientes\CitasController;

// Citas
    Route::get('/citas',                      [CitasController::class, 'index']);
    Route::get('/citas/buscar/{txt}',           [CitasController::class, 'search']);
    Route::get('/citas/list/{txt}',              [CitasController::class, 'list']);
    Route::post('/citas/filtrar',                [CitasController::class, 'filter']);
    Route::get('/cita/{id}',                    [CitasController::class, 'read']);
    Route::post('/cita',                        [CitasController::class, 'store']);
    Route::delete('/cita/{id}',                 [CitasController::class, 'delete']);
    
?>
