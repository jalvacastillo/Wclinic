<?php

namespace App\Http\Controllers\Api\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Empleados\Empleado;

class EmpleadosController extends Controller
{
    

    public function index() {
       
        $empleados = Empleado::where('empleado', true)->with('contrato')->orderBy('id','desc')->paginate(200);

        return Response()->json($empleados, 200);

    }

    public function list() {
       
        $empleados = Empleado::where('empleado', true)->with('contrato')->orderBy('id','desc')->get();

        return Response()->json($empleados, 200);

    }


    public function read($id) {
        
        $empleado = Empleado::where('id', $id)->with('contrato')->firstOrFail();

        return Response()->json($empleado, 200);
    }

    public function filter(Request $request) {
        
        $empleado = Empleado::when($request->sucursal_id, function($query) use ($request){
                                    return $query->where('sucursal_id', $request->sucursal_id);
                                })
                                ->when($request->tipo, function($query) use ($request){
                                    return $query->where('tipo', $request->tipo);
                                })
                                ->orderBy('id','desc')->paginate(100000);

        return Response()->json($empleado, 200);
    }

    public function search($txt) {

        $empleados = Empleado::where('name', 'like' ,'%' . $txt . '%')->paginate(200);
        return Response()->json($empleados, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'name'   => 'required|max:255',
            'username'  => 'sometimes|unique:users,username,'.$request->id
        ]);

        if ($request->password) {
            $request->validate([
                'password' => 'required|string|min:3|confirmed'
            ]);
        }

        if($request->id)
            $empleado = Empleado::findOrFail($request->id);
        else
            $empleado = new Empleado;


        if ($request->password) {
            $request['password'] = \Hash::make($request->password);
        }
       
        if (!$request->id) {
            $request['password'] = \Hash::make('emple');
        }
        
        $empleado->fill($request->all());
        $empleado->save();

        return Response()->json($empleado, 200);


    }

    public function delete($id)
    {
       
        $empleado = Empleado::findOrFail($id);
        $empleado->delete();

        return Response()->json($empleado, 201);

    }

    public function caja($id)
    {
       
        $empleados = Empleado::where('caja_id', $id)->get();

        return Response()->json($empleados, 200);

    }

    public function validar(Request $request)
    {

        $supervisor = Empleado::where('codigo', $request->codigo)->first();
        
        if (!$supervisor)
            return Response()->json(['error' => ['Datos incorrectos'], 'code' => 422], 422);

        return Response()->json($supervisor, 200); 

    }

    public function auth(Request $request)
    {
        
        $user = Empleado::where('username', $request->username)->firstOrFail();

        if (!Hash::check($request->password, $user->password))
            return Response()->json(['error' => ['Datos incorrectos'], 'code' => 422], 422);

        return Response()->json($user, 200); 

    }

    public function comisiones($id)
    {
        
        $empleado = Empleado::where('id', $id)->with('comisiones')->firstOrFail();

        return Response()->json($empleado, 200); 

    }



}
