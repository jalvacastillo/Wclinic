<?php

namespace App\Http\Controllers\Api\Empleados;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Empleados\Contrato;
use Carbon\Carbon;

class ContratosController extends Controller
{
    

    public function store(Request $request) {
        
        $request->validate([
            'empleado_id'   => 'required|numeric',
            'fecha'         => 'required',
            'dias'          => 'required|numeric',
            'horas'         => 'required|numeric',
            'sueldo'        => 'required|numeric',
            'renta'         => 'sometimes|boolean',
            'isss'          => 'sometimes|boolean',
            'afp'           => 'sometimes|boolean',
        ]);

        if($request->id)
            $contrato = Contrato::findOrFail($request->id);
        else
            $contrato = new Contrato;
        
        $contrato->fill($request->all());
        $contrato->save();

        return Response()->json($contrato, 200);


    }

    public function delete($id)
    {
       
        $contrato = Contrato::findOrFail($id);
        $contrato->delete();

        return Response()->json($contrato, 201);

    }



}
