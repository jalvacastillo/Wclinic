<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Pacientes\Examen;

class ExamenesController extends Controller
{
    

    public function index() {
       
        $examenes = Examen::orderBy('id','desc')->paginate(10);
       
        return Response()->json($examenes, 200);

    }



    public function read($id) {

        $examen = Examen::where('id', $id)->with('detalles', 'paciente')->first();
        return Response()->json($examen, 200);

    }

    public function search($txt) {

        $examenes = Examen::whereHas('paciente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('metodo_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($examenes, 200);

    }

    public function filter(Request $request) {


        $examenes = Examen::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($examenes, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'examen_id'       => 'required',
            'paciente_id'       => 'required',
            'usuario_id'        => 'required',
            'sucursal_id'       => 'required',
        ]);

        if($request->id)
            $examen = Examen::findOrFail($request->id);
        else
            $examen = new Examen;
        
        $examen->fill($request->all());
        $examen->save();        

        return Response()->json($examen, 200);

    }

    public function delete($id)
    {
        $examen = Examen::findOrFail($id);
        $examen->delete();

        return Response()->json($examen, 201);

    }




}
