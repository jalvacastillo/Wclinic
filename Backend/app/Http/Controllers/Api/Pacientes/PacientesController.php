<?php

namespace App\Http\Controllers\Api\Pacientes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pacientes\Paciente;
use App\Models\Pacientes\Anticipo;
use App\Models\Ventas\Venta;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class PacientesController extends Controller
{
    

    public function index() {
       
        $pacientes = Paciente::orderBy('created_at','desc')
                    ->paginate(10);

        foreach ($pacientes as $paciente) {
            $paciente->num_consultas = $paciente->consultas()->count();
        }

        return Response()->json($pacientes, 200);

    }


    public function search($txt) {

        $paciente = Paciente::where('nombre', 'like' ,'%' . $txt . '%')
                            ->orderBy('nombre','asc')->paginate(10);
        return Response()->json($paciente, 200);

    }

    public function filter(Request $request) {

        if ($request->pagos != '') {
            if ($request->pagos == 1) {
                $paciente = Paciente::where('id','!=', 1)
                                    ->wherehas('ventas', function($q){
                                        $q->where('estado', 'Pendiente');
                                    })->orderBy('id','desc')->paginate(1000);
            }else{
                $paciente = Paciente::where('id','!=', 1)
                                    ->whereDoesntHave('ventas', function($q){
                                        $q->where('estado', 'Pendiente');
                                    })->orderBy('id','desc')->paginate(1000);
            }
        }

        foreach ($paciente as $paciente) {
            $anticipos = $paciente->anticipos();
            $paciente->num_ventas = $paciente->ventas->count();
            $paciente->num_ventas_pendientes = $paciente->ventasPendientes->count();
            
            $paciente->pago_pendiente = $paciente->ventasPendientes->sum('total');
            $abonos = $anticipos->where('tipo', 'Abono')->sum('total');
            $cargos = $anticipos->where('tipo', 'Cargo')->sum('total');
            $paciente->pago_anticipado = $abonos - $cargos;
        }

        return Response()->json($paciente, 200);

    }

    public function read($id) {

        $paciente = Paciente::findOrFail($id);
        $paciente->num_consultas = $paciente->consultas()->count();
        return Response()->json($paciente, 200);

    }

    public function store(Request $request)
    {

        $request->validate([
            'nombre'    => 'required|max:255',
            'fecha' => 'sometimes|max:255',
            'nombre'    => 'sometimes|max:255',
            'dui'  => 'sometimes|max:255',
            'nit'  => 'sometimes|max:255',
            'fecha_nacimiento'  => 'sometimes|date',
            'direccion' => 'sometimes|max:255',
            'municipio' => 'sometimes|max:255',
            'departamento'  => 'sometimes|max:255',
            'telefono'  => 'sometimes|max:255',
            'correo'    => 'sometimes|max:255',
            'sexo'  => 'sometimes|max:255',
            'profesion' => 'sometimes|max:255',
            'estado_civil'  => 'sometimes|max:255',
            'citologia' => 'sometimes|max:255',
            'responsable'   => 'sometimes|max:255',
            'madre' => 'sometimes|max:255',
            'padre' => 'sometimes|max:255',
            'alergias'  => 'sometimes|max:255',
            'antecedentes'  => 'sometimes|max:255',
            'medicacion_actual' => 'sometimes|max:255',
            'etiquetas' => 'sometimes|max:255',
            'img'  => 'sometimes|max:255',
            'file' => 'sometimes|image|mimes:jpeg,png,jpg|max:1024',
            'nota'  => 'sometimes|max:500',
            'empresa_id'    => 'sometimes|numeric',
        ]);

        if($request->id)
            $paciente = Paciente::findOrFail($request->id);
        else
            $paciente = new Paciente;
        
        $paciente->fill($request->all());

        if ($request->hasFile('file')) {
            if ($request->id && $paciente->img) {
                Storage::delete($paciente->img);
            }
           $nombre = $request->file->store('pacientes');
           $paciente->img = $nombre;
        }

        $paciente->save();

        return Response()->json($paciente, 200);

    }

    public function delete($id)
    {
        $paciente = Paciente::findOrFail($id);
        if ($paciente->ventas()->count() == 0) {
            if ($paciente->img)
                Storage::delete($paciente->img);
            $paciente->delete();
        }

        return Response()->json($paciente, 201);

    }

    public function expediente($id)
    {
        $paciente = Paciente::where('id', $id)->with('empresa')->firstOrFail();

        $view = \PDF::loadView('reportes.pacientes.expediente', compact('paciente'));
        return $view->stream();

    }

    public function ventas($id) {

        $ventas = Venta::where('paciente_id', $id)
                        ->where('estado', '!=', 'Anulada')
                        ->orderBy('id', 'desc')
                        ->paginate(10);
        return Response()->json($ventas, 200);

    }

    public function ventasFilter(Request $request) {

        if ($request->estado == 'Anulada') {
            $ventas = Venta::where('paciente_id', $request->id)
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->forma_de_pago, function($query) use ($request){
                            return $query->where('forma_de_pago', $request->forma_de_pago);
                        })
                        ->orderBy('id','desc')->paginate(100000);
        }else{

            $ventas = Venta::where('paciente_id', $request->id)
                        ->where('estado', '!=', 'Anulada')
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->forma_de_pago, function($query) use ($request){
                            return $query->where('forma_de_pago', $request->forma_de_pago);
                        })
                        ->orderBy('id','desc')->paginate(100000);
        }

        return Response()->json($ventas, 200);

    }

    public function valesPendientes($id) {

        $ventas = Venta::where('paciente_id', $id)->where('estado', 'Pendiente')
                        ->where('forma_de_pago', 'Vale')
                        ->orderBy('id', 'desc')->get();
        return Response()->json($ventas, 200);

    }

    public function anticipos($id){

        $anticipos = Anticipo::where('paciente_id', $id)
                    ->orderBy('id','desc')->paginate(10);
        return Response()->json($anticipos, 200);

    }


    public function cxc() {
       
        $paciente = Paciente::where('id','!=', 1)
                        ->whereRaw('paciente.id in (select paciente_id from ventas where estado = ?)', ['Pendiente'])
                        ->paginate(10);

        foreach ($paciente as $paciente) {
            $paciente->num_ventas_pendientes = $paciente->ventasPendientes->count();
            $paciente->pago_pendiente = $paciente->ventasPendientes->sum('total');
        }

        return Response()->json($paciente, 200);

    }

    public function cxcBuscar($txt) {
       
        $paciente = Paciente::where('id','!=', 1)->where('nombre', 'like' ,'%' . $txt . '%')
                        ->orWhere('registro', 'like' , $txt . '%')
                        ->orWhereRaw('REPLACE(registro, "-", "") like "'.$txt.'"')
                        ->whereRaw('paciente.id in (select paciente_id from ventas where estado = ?)', ['Pendiente'])
                        ->paginate(10);

        return Response()->json($paciente, 200);

    }




}
