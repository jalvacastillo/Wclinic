<?php

namespace App\Http\Controllers\Api\Pacientes\Examenes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pacientes\Consultas\Examenes\Examen;

class ExamenesController extends Controller
{
    

    public function index() {
       
        $examenes = Examen::orderBy('id','desc')->paginate(10);

        return Response()->json($examenes, 200);

    }


    public function read($id) {
        
        $examen = Examen::where('id', $id)->firstOrFail();
        return Response()->json($examen, 200);
    }

    public function filter($campo, $valor) {
        
        $examen = Examen::where($campo, $valor)->paginate(10);

        return Response()->json($examen, 200);
    }

    public function search($txt) {
        $examenes = Examen::orwhere('nombre', 'like' ,'%' . $txt . '%')
                        ->orwhere('especialidad', 'like' ,'%' . $txt . '%')
                        ->orwhere('etiquetas', 'like' ,'%' . $txt . '%')
                        ->paginate(10000);


        return Response()->json($examenes, 200);

    }


    public function store(Request $request)
    {
        $request->validate([
            'code'          => 'required|max:255',
            'descripcion'   => 'required|max:500',
            'equivalencias' => 'sometimes|max:500',
            'empresa_id'    => 'required|numeric',
        ]);

        $examen->fill($request->all());
        $examen->save();

        return Response()->json($examen, 200);


    }

    public function delete($id)
    {
       
        $examen = Examen::findOrFail($id);
        $examen->delete();

        return Response()->json($examen, 201);

    }



}
