<?php

namespace App\Http\Controllers\Api\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use JWTAuth;
use Carbon\Carbon;

use App\Models\Ventas\Venta;
use App\Models\Ventas\DetalleCombo;
use App\Models\Admin\Empresa;

use App\Models\Admin\Caja;
use App\Models\Admin\Mesa;
use App\Models\Registros\Cliente;
use App\Models\Admin\Documento;
use App\Models\Ventas\Detalle;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Kardex;
use App\Models\Inventario\Inventario;

use App\Models\Creditos\Credito;

use Illuminate\Support\Facades\DB;

class VentasController extends Controller
{
    

    public function index() {
       
        $ventas = Venta::orderBy('id','desc')->paginate(10);
       
        return Response()->json($ventas, 200);

    }



    public function read($id) {

        $venta = Venta::where('id', $id)->with('detalles', 'cliente', 'credito')->first();

        return Response()->json($venta, 200);

    }

    public function search($txt) {

        $ventas = Venta::whereHas('cliente', function($query) use ($txt) {
                                    $query->where('nombre', 'like' ,'%' . $txt . '%');
                                })
                                ->orwhere('correlativo', 'like', '%'.$txt.'%')
                                ->orwhere('tipo_documento', 'like', '%'.$txt.'%')
                                ->orwhere('estado', 'like', '%'.$txt.'%')
                                ->orwhere('nota', 'like', '%'.$txt.'%')
                                ->orwhere('metodo_pago', 'like', '%'.$txt.'%')
                                ->orwhere('referencia', 'like', '%'.$txt.'%')
                                ->paginate(10);

        return Response()->json($ventas, 200);

    }

    public function filter(Request $request) {


        $ventas = Venta::when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->sucursal_id, function($query) use ($request){
                            return $query->where('sucursal_id', $request->sucursal_id);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->when($request->estado, function($query) use ($request){
                            return $query->where('estado', $request->estado);
                        })
                        ->when($request->metodo_pago, function($query) use ($request){
                            return $query->where('metodo_pago', $request->metodo_pago);
                        })
                        ->when($request->tipo_documento, function($query) use ($request){
                            return $query->where('tipo_documento', $request->tipo_documento);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($ventas, 200);

    }

    public function store(Request $request)
    {
        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required',
            'cliente_id'        => 'required',
            'usuario_id'        => 'required',
        ]);

        if($request->id)
            $venta = Venta::findOrFail($request->id);
        else
            $venta = new Venta;
        
        $venta->fill($request->all());
        $venta->save();        

        return Response()->json($venta, 200);

    }

    public function delete($id)
    {
        $venta = Venta::findOrFail($id);

        foreach ($venta->detalles as $detalle) {
            $detalle->delete();
        }
        $venta->delete();

        return Response()->json($venta, 201);

    }



    // Facturacion

    public function corte() {

        $usuario = JWTAuth::parseToken()->authenticate();
       
        $caja   = Caja::where('id', $usuario->caja_id)->with('corte')->firstOrFail();
        $corte  = $caja->corte;
        $ventas = $corte->ventas()->orderBy('id', 'desc')
                            ->paginate(30);

        return Response()->json($ventas, 200);

    }

    public function facturacion(Request $request){

        $request->validate([
            'fecha'             => 'required',
            'estado'            => 'required|max:255',
            'tipo_documento'    => 'required|max:255',
            'metodo_pago'       => 'required|max:255',
            'cliente'           => 'required',
            'detalles'          => 'required',
            'iva'               => 'required|numeric',
            'subcosto'          => 'required|numeric',
            'plazo'             => 'required_if:metodo_pago,"Credito"',
            'subtotal'          => 'required|numeric',
            'total'             => 'required|numeric',
            'nota'              => 'max:255',
            'caja_id'           => 'required|numeric',
            'corte_id'          => 'required|numeric',
            'usuario_id'        => 'required|numeric',
            'sucursal_id'       => 'required|numeric',
        ]);

        DB::beginTransaction();
         
        try {
        
        // Guardamos el cliente

            if(isset($request->cliente['id']))
                $cliente = Cliente::findOrFail($request->cliente['id']);
            else
                $cliente = new Cliente;

            $cliente->fill($request->cliente);
            $cliente->save();

        // Guardamos la venta
            if($request->id)
                $venta = Venta::findOrFail($request->id);
            else
                $venta = new Venta;
            $request['cliente_id'] = $cliente->id;
            $venta->fill($request->all());
            $venta->save();

        // Guardamos crédito
        if ($request->metodo_pago == 'Credito') {
            $credito = new Credito;
            $credito->fecha         = date('Y-m-d');
            $credito->venta_id      = $venta->id;
            $credito->total         = $venta->total;
            $credito->interes       = 0;
            $credito->plazo         = $request->plazo;
            $credito->prima         = 0;
            $credito->usuario_id    = $venta->usuario_id;
            $credito->cliente_id    = $venta->cliente_id;
            $credito->empresa_id    = $venta->sucursal()->first()->empresa_id;
            $credito->save();
        }

        // Guardamos los detalles

            foreach ($request->detalles as $det) {
                if(isset($det['id']))
                    $detalle = Detalle::findOrFail($det['id']);
                else
                    $detalle = new Detalle;
                $det['venta_id'] = $venta->id;
                

                $detalle->fill($det);
                $detalle->save();

                // Actualizar inventario
                $producto = Producto::findOrFail($det['producto_id']);

                if ($producto->compuesto && $producto->composiciones()->count() > 0) {
                    
                    foreach ($producto->composiciones()->get() as $comp) {
                        $productoCompuesto = $comp->compuesto()->first();
                        if ($productoCompuesto && $productoCompuesto->inventario  && !$request->id) {
                            $inventario = Inventario::where('producto_id', $comp->compuesto_id)->where('bodega_id', $productoCompuesto->bodega_venta_id)->first();
                            if ($inventario) {
                                $inventario->stock -= $det['cantidad'] * $comp->cantidad;
                                $inventario->save();
                            // Kardex
                                $valor = $productoCompuesto->tipo == 'Producto' ? $productoCompuesto->precio : $productoCompuesto->costo;
                                $entradaCantidad =  null;
                                $salidaCantidad =  $det['cantidad'] * $comp->cantidad;
                                Kardex::create([
                                    'fecha'             => date('Y-m-d'),
                                    'producto_id'       => $comp->compuesto_id,
                                    'bodega_id'         => $producto->bodega_venta_id,
                                    'detalle'           => 'Venta',
                                    'referencia'        => $venta->id,
                                    'valor_unitario'    => $valor,
                                    'entrada_cantidad'  => $entradaCantidad,
                                    'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                    'salida_cantidad'   => $salidaCantidad,
                                    'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                    'total_cantidad'    => $inventario->stock,
                                    'total_valor'       => $inventario->stock * $valor,
                                    'usuario_id'        => $request->usuario_id,
                                ]);
                            }

                        }
                    }


                }

                if ($producto->inventario && !$request->id) {
                    $inventario = Inventario::where('producto_id', $producto->id)->where('bodega_id', $producto->bodega_venta_id)->first();
                        // Kardex
                    if ($inventario) {
                        $inventario->stock -= $det['cantidad'];
                        $inventario->save();
                            $valor = $producto->tipo == 'Producto' ? $producto->precio : $producto->costo;
                            $entradaCantidad =  null;
                            $salidaCantidad =  $det['cantidad'];
                            Kardex::create([
                                'fecha'             => date('Y-m-d'),
                                'producto_id'       => $producto->id,
                                'bodega_id'         => $producto->bodega_venta_id,
                                'detalle'           => 'Venta',
                                'referencia'        => $venta->id,
                                'valor_unitario'    => $valor,
                                'entrada_cantidad'  => $entradaCantidad,
                                'entrada_valor'     => $entradaCantidad ? $entradaCantidad * $valor : null,
                                'salida_cantidad'   => $salidaCantidad,
                                'salida_valor'      => $salidaCantidad ? $salidaCantidad * $valor : null,
                                'total_cantidad'    => $inventario->stock,
                                'total_valor'       => $inventario->stock * $valor,
                                'usuario_id'        => $request->usuario_id,
                            ]);
                    }
                }
                
            }
            

        // Incrementar el correlarivo
            Documento::where('caja_id', JWTAuth::parseToken()->authenticate()->caja_id)
                                ->where('nombre', $request->tipo_documento)
                                ->increment('actual');
        
        DB::commit();
        return Response()->json($venta, 200);

        } catch (\Exception $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        } catch (\Throwable $e) {
            DB::rollback();
            return Response()->json(['error' => $e->getMessage()], 400);
        }
        

    }

    public function ordenes() {

        $usuario = JWTAuth::parseToken()->authenticate();
       
        $caja    = Caja::where('id', $usuario->caja_id)->with('corte')->firstOrFail();
        $corte   = $caja->corte;
        
        if (!$corte->cierre)
            $corte->cierre = Carbon::now()->toDateTimeString(); ;

        $ventas  = $corte->ventas()->where('estado', 'Pendiente')
                            ->orderBy('id', 'desc')
                            ->paginate(5000);
        

        return Response()->json($ventas, 200);


    }

    public function propinas(Request $request) {

        $ventas = Venta::where('propina', '>', 0)->when($request->inicio, function($query) use ($request){
                            return $query->whereBetween('fecha', [$request->inicio, $request->fin]);
                        })
                        ->when($request->sucursal_id, function($query) use ($request){
                            return $query->where('sucursal_id', $request->sucursal_id);
                        })
                        ->when($request->usuario_id, function($query) use ($request){
                            return $query->where('usuario_id', $request->usuario_id);
                        })
                        ->orderBy('id','desc')->paginate(100000);

        return Response()->json($ventas, 200);

    }

    public function generarDoc($id){
        $venta = Venta::where('id', $id)->with('detalles', 'cliente')->firstOrFail();

        $empresa = Empresa::find(1);

        $partes = explode('.', strval( number_format($venta->total, 2) ));

        $venta->total_letras = \NumeroALetras::convertir($partes[0], 'Dolares con ') . $partes[1].'/100';

        if ($venta->tipo_documento == 'Factura') {

            return view('reportes.factura', compact('venta', 'empresa'));
        }
        elseif ($venta->tipo_documento == 'Credito Fiscal') {

            return view('reportes.credito', compact('venta', 'empresa'));

        }elseif ($venta->tipo_documento == 'Ticket') {

            return view('reportes.ticket', compact('venta', 'empresa'));
        }
        else{
            return "Venta sin tipo";
        }

    }

    public function anularDoc(){

        return view('reportes.anulacion');

    }

    public function libroIva(Request $request) {
        $star = $request->inicio;
        $end = $request->fin;

        $ventas = Venta::where('tipo_documento', 'Credito Fiscal')
                            ->where('estado', '!=', 'Pendiente')
                            ->whereBetween('fecha', [$request->inicio, $request->fin])
                            ->orderBy('fecha','desc')->get();

        $ivas = collect();

        foreach ($ventas as $venta) {
                $ivas->push([
                    'fecha'         => $venta->fecha,
                    'correlativo'   => $venta->correlativo,
                    'cliente'       => $venta->estado == 'Anulada' ?  'ANULADA': $venta->cliente_nombre,
                    'registro'      => $venta->registro,
                    'interno'       => $venta->subtotal,
                    'iva'           => $venta->iva,
                    'fovial'        => $venta->fovial,
                    'cotrans'       => $venta->cotrans,
                    'total'         => $venta->total
                ]);
        }

        $ivas = $ivas->sortByDesc('correlativo')->values()->all();

        return Response()->json($ivas, 200);

    }

    public function cxc() {
       
        $cobros = Venta::where('estado', 'Pendiente')->orderBy('fecha','desc')->paginate(10);

        return Response()->json($cobros, 200);

    }

    public function cxcBuscar($txt) {
       
        $cobros = Venta::where('estado', 'Pendiente')
                        ->whereHas('cliente', function($query) use ($txt) {
                            $query->where('nombre', 'like' ,'%' . $txt . '%');
                        })
                        ->orderBy('fecha','desc')->paginate(10);

        return Response()->json($cobros, 200);

    }

    public function historial(Request $request) {

        $ventas = Venta::where('estado', 'Cobrada')->whereBetween('fecha', [$request->inicio, $request->fin])
                        ->get()
                        ->groupBy(function($date) {
                            return Carbon::parse($date->fecha)->format('d-m-Y');
                        });
        
        $movimientos = collect();

        foreach ($ventas as $venta) {
            $ventaTotal = $venta->sum('total');
            $costoTotal = $venta->sum('subcosto');
            $movimientos->push([
                'cantidad'      => $venta->count(),
                'fecha'         => $venta[0]->fecha,
                'total'         => $ventaTotal,
                'costo'         => $costoTotal,
                'utilidad'      => $ventaTotal - $costoTotal,
                'detalles'      => $venta
            ]);
        }

        return Response()->json($movimientos, 200);

    }


}
