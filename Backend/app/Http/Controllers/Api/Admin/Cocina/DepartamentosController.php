<?php

namespace App\Http\Controllers\Api\Admin\Cocina;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Admin\Cocina\Departamento;

class DepartamentosController extends Controller
{
    

    public function index() {
       
        $departamentos = Departamento::orderBy('created_at', 'desc')->paginate(12);
        return Response()->json($departamentos, 200);

    }


    public function read($id) {
        
        $departamento = Departamento::where('id', $id)->with('detalles')->firstOrFail();
        return Response()->json($departamento, 200);

    }


    public function store(Request $request)
    {

        $request->validate([
            'nombre'        => 'required|max:255',
            'sucursal_id'    => 'required|numeric',
        ]);

        if($request->id)
            $departamento = Departamento::findOrFail($request->id);
        else
            $departamento = new Departamento;
        
        $departamento->fill($request->all());
        $departamento->save();

        return Response()->json($departamento, 200);

    }


    public function delete($id)
    {
       
        $departamento = Departamento::findOrFail($id);
        foreach ($departamento->detalles() as $detalle) {
            $detalle->delete();
        }
        $departamento->delete();

        return Response()->json($departamento, 201);

    }


}
