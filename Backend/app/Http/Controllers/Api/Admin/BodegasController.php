<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use JWTAuth;
use App\Models\Admin\Empresa;
use App\Models\Admin\Bodega;

class BodegasController extends Controller
{
    

    public function index() {
       
        $bodegas = Bodega::all();

        return Response()->json($bodegas, 200);

    }


    public function read($id) {
        
        $bodega = Bodega::where('id', $id)->firstOrFail();

        $productos = $bodega->productos()->get();
        $bodega->productos  = $productos->count();
        $bodega->cantidad   = $productos->sum('stock');

        return Response()->json($bodega, 200);

    }

    public function store(Request $request) {
    	
        $request->validate([
            'nombre'       => 'required|max:255',
            'descripcion'  => 'sometimes|max:255',
            'sucursal_id'  => 'required|numeric',
        ]);

        if($request->id)
            $bodega = Bodega::findOrFail($request->id);
        else
            $bodega = new Bodega;
        
        $bodega->fill($request->all());
        $bodega->save();

        return Response()->json($bodega, 200);


    }

    public function delete($id)
    {
       
        $bodega = Bodega::findOrFail($id);
        if ($bodega->productos()->count() > 0)
            return  Response()->json(['message' => 'La bodega tiene productos', 'code' => 402], 402);
        $bodega->delete();

        return Response()->json($bodega, 201);

    }

    public function reporte($id, $cat) {

        $bodega = Bodega::where('id', $id)->firstOrFail();
        $productos = [];

        $categorias = explode(',', $cat);

        $productos = $bodega->productos()->with('producto')
        // ->when($categorias, function($query) use ($categorias){
        //     $query->whereHas('producto', function($query) use ($categorias){
        //         return $query->whereIn('categoria_id', $categorias);
        //     });
        // })
        ->get();

        $bodega->fecha = Carbon::now();
        $bodega->usuario = JWTAuth::parseToken()->authenticate()->name;
        $empresa = Empresa::find(JWTAuth::parseToken()->authenticate()->empresa_id);

        $p = collect();

        foreach ($productos as $producto) {
            $prod = $producto->producto()->first();
            $stock = $prod->inventarios()->where('bodega_id', $id)->first()->stock;
            $p->push([
                'nombre'        => $prod->nombre,
                'categoria'     => $prod->nombre_categoria,
                'tipo'          => $prod->tipo,
                'stock'         => $stock,
                'costo'         => $prod->costo,
                'costoTotal'    => $prod->costo * $stock,
                'precio'        => $prod->precio,
                'precioTotal'   => $prod->precio * $stock,
            ]);
        }

        $bodega->productos = $p->sortBy('categoria');
        // return $bodega;
        $reportes = \PDF::loadView('reportes.bodegas', compact('bodega', 'empresa'));
        return $reportes->stream();

    }


}
