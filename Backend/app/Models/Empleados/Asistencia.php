<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Asistencia extends Model {

    protected $table = 'empleados_asistencias';
    protected $fillable = array(
        'entrada',
        'salida',
        'ubicacion',
        'usuario_id'
    );

    protected $appends = ['nombre_empleado', 'horas_laborales', 'horas_extras'];

    public function getHorasLaboralesAttribute()
    {
        if ($this->entrada)
            if ($this->salida)
                return Carbon::parse($this->entrada)->diffInHours(Carbon::parse($this->salida));
            else
                return Carbon::parse($this->entrada)->diffInHours(Carbon::now());
        else
            return 0;
    }

    public function getHorasExtrasAttribute()
    {
        $horas = $this->empleado()->first()->contrato()->pluck('horas')->first();

        if ($this->horas_laborales > $horas)
            return $this->horas_laborales - $horas;
        else
            return 0;
    }

    public function getNombreEmpleadoAttribute()
    {
        return $this->empleado()->pluck('name')->first();
    }

    public function empleado(){
        return $this->belongsTo('App\Models\Empleados\Empleado', 'usuario_id');
    }


}



