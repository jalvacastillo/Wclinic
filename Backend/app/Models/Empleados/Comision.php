<?php

namespace App\Models\Empleados;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Comision extends Model {

    protected $table = 'empleado_comisiones';
    protected $fillable = array(
        'fecha',
        'concepto',
        'tipo',
        'estado',
        'nota',
        'total',
        'venta_id',
        'empleado_id',
        'usuario_id',
    );

    protected $appends = ['nombre_empleado', 'nombre_usuario'];

    public function getNombreEmpleadoAttribute()
    {
        return $this->empleado()->pluck('name')->first();
    }
    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function empleado(){
        return $this->belongsTo('App\Models\Empleados\Empleado', 'empleado_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }

    public function venta(){
        return $this->belongsTo('App\Models\Ventas\Venta', 'venta_id');
    }


}