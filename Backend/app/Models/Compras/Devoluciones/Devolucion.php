<?php

namespace App\Models\Compras\Devoluciones;

use Illuminate\Database\Eloquent\Model;

class Devolucion extends Model {

    protected $table = 'compras_devoluciones';
    protected $fillable = array(
        'fecha',
        'tipo',
        'referencia',
        'proveedor_id',
        'iva_retenido',
        'descuento',
        'iva',
        'subtotal',
        'total',
        'nota',
        'usuario_id',
        'empresa_id',
    );

    protected $appends = ['detalles_num', 'nombre_proveedor', 'nombre_usuario', 'exenta', 'gravada', 'no_sujeta'];


    public function getDetallesNumAttribute()
    {
        return $this->detalles()->count();
    }

    public function getExentaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('excenta');
        return $interno;
    }

    public function getGravadaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('gravada');
        return $interno;
    }

    public function getNoSujetaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('no_sujeta');
        return $interno;
    }

    public function getNombreProveedorAttribute()
    {
        return $this->proveedor()->pluck('nombre')->first();
    }

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function proveedor(){
        return $this->belongsTo('App\Models\Registros\Proveedor','proveedor_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

    public function detalles(){
        return $this->hasMany('App\Models\Compras\Devoluciones\Detalle','compra_id');
    }


}
