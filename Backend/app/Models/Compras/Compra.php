<?php

namespace App\Models\Compras;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model {

    protected $table = 'compras';
    protected $fillable = array(
        'fecha',
        'estado',
        'tipo',
        'referencia',
        'proveedor_id',
        'fecha_pago',
        'iva_retenido',
        'descuento',
        'iva',
        'subtotal',
        'total',
        'usuario_id',
        'empresa_id',
    );

    protected $appends = ['detalles_num', 'proveedor', 'usuario', 'exenta', 'gravada', 'no_sujeta'];


    public function getDetallesNumAttribute()
    {
        return $this->detalles()->count();
    }

    public function getExentaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('excenta');
        return $interno;
    }

    public function getGravadaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('gravada');
        return $interno;
    }

    public function getNoSujetaAttribute()
    {
        $detalles = $this->detalles()->get();
        $interno = $detalles->sum('no_sujeta');
        return $interno;
    }

    public function getProveedorAttribute()
    {
        return $this->proveedor()->pluck('nombre')->first();
    }

    public function getUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function proveedor(){
        return $this->belongsTo('App\Models\Registros\Proveedor','proveedor_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

    public function detalles(){
        return $this->hasMany('App\Models\Compras\Detalle','compra_id');
    }


}
