<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use JWTAuth;

class Venta extends Model {

    protected $table = 'ventas';
    protected $fillable = array(
        'fecha',
        'correlativo',
        'estado',
        'tipo',
        'metodo_pago',
        'tipo_documento',
        'referencia',
        'recibido',
        'iva_retenido',
        'iva',
        'subcosto',
        'descuento',
        'subtotal',
        'total',
        'nota',
        'caja_id',
        'corte_id',
        'cliente_id',
        'usuario_id',
        'sucursal_id'
    );

    protected $appends = ['nombre_cliente', 'nombre_usuario', 'exenta', 'gravada', 'no_sujeta'];

    protected static function booted()
    {
        $usuario = auth()->user() ? JWTAuth::parseToken()->authenticate() : null;

        if ($usuario && $usuario->tipo != 'Administrador'){
            static::addGlobalScope('sucursal', function (Builder $builder) use ($usuario) {
                $builder->where('sucursal_id', $usuario->sucursal_id);
            });
        }
    }


    public function getNombreClienteAttribute()
    {
        return $this->cliente()->first() ? $this->cliente()->pluck('nombre')->first() : '';
    }

    public function getNombreAttribute($name)
    {
        return strtoupper($name);
    }


    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getExentaAttribute(){
        return $this->detalles()->get()->sum('Exenta');
    }

    public function getGravadaAttribute(){
        return $this->detalles()->get()->sum('Gravada');
    }

    public function getNoSujetaAttribute(){
        return $this->detalles()->get()->sum('No Sujeta');
    }

    // Relaciones

    public function cliente(){
        return $this->belongsTo('App\Models\Ventas\Clientes\Cliente','cliente_id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\User','usuario_id');
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal','sucursal_id');
    }

    public function credito(){
        return $this->hasOne('App\Models\Creditos\Credito','venta_id');
    }

    public function detalles(){
        return $this->hasMany('App\Models\Ventas\Detalle','venta_id');
    }


}
