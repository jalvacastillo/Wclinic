<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use JWTAuth;

class Servicio extends Model {

    use SoftDeletes;
    protected $table = 'productos';
    protected $fillable = array(
        'nombre',
        'descripcion',
        'codigo',
        'medida',
        'precio',
        'precio2',
        'precio3',
        'precio4',
        'costo',
        'costo_anterior',
        'categoria_id',
        'tipo',
        'tipo_impuesto',
        'compuesto',
        'activo',
        'nota',
        'empresa_id',
    );

    protected $appends = ['img', 'nombre_categoria', 'nombre_subcategoria', 'costo_promedio', 'promocion'];
    protected $casts = ['inventario' => 'boolean', 'compuesto' => 'boolean', 'activo' => 'boolean'];

    protected static function booted()
    {
        $usuario = auth()->user() ? JWTAuth::parseToken()->authenticate() : null;

        if ($usuario && $usuario->tipo != 'Administrador') {
            static::addGlobalScope('sucursal', function (Builder $builder) use ($usuario) {
                $builder->wherehas('sucursales', function($q) use ($usuario){
                    $q->where('sucursal_id', $usuario->sucursal_id);
                });
            });
        }
    }

    public function getNombreAttribute($value)
    {
        return strtoupper($value);
    }

    public function getNombreCategoriaAttribute()
    {
        return $this->categoria()->first() ? $this->categoria()->first()->categoria()->pluck('nombre')->first() : '';
    }

    public function getNombreSubcategoriaAttribute()
    {
        return $this->categoria()->pluck('nombre')->first();
    }

    public function getCostoPromedioAttribute()
    {
        return number_format(($this->costo + $this->costo_anterior) / 2, 2);
    }

    public function getPromocionAttribute()
    {
        return $this->promociones()->where('inicio', '<', \Carbon\Carbon::now())
                                ->where('fin', '>', \Carbon\Carbon::now())
                                ->latest()
                                ->first();
    }


    public function getImgAttribute()
    {
        return $this->imagenes()->orderBy('id', 'asc')->pluck('img')->first();
    }

    public function categoria(){
        return $this->belongsTo('App\Models\Inventario\Categorias\SubCategoria','categoria_id');
    }

    public function sucursales(){
        return $this->hasMany('App\Models\Inventario\Sucursal','producto_id');
    }

    public function composiciones(){
        return $this->hasMany('App\Models\Inventario\Composicion','producto_id');
    }

    public function promociones(){
        return $this->hasMany('App\Models\Inventario\Promocion','producto_id');
    }

    public function imagenes(){
        return $this->hasMany('App\Models\Inventario\Imagen','producto_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Detalle','producto_id');
    }


}



