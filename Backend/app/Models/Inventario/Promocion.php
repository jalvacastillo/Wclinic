<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Promocion extends Model {

    protected $table = 'producto_promociones';
    protected $fillable = array(
        'producto_id',
        'precio',
        'inicio',
        'fin'
    );

    protected $appends = ['activa'];

    public function getActivaAttribute($value)
    {
        if ($this->fin >= Carbon::now())
            return true;
        else
            return false;
    }

    public function getInicioAttribute($value)
    {
        return Carbon::parse($value);
    }

    public function getFinAttribute($value)
    {
        return Carbon::parse($value);
    }
   
    public function producto(){
        return $this->hasMany('App\Models\Inventario\Producto','producto_id');
    }

}



