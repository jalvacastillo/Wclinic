<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model {

    protected $table = 'producto_sucursales';
    protected $fillable = array(
        'producto_id',
        'inventario',
        'bodega_venta_id',
        'activo',
        'sucursal_id',
    );

    protected $appends = ['nombre_producto', 'nombre_sucursal'];
    protected $casts = ['inventario' => 'boolean', 'activo' => 'boolean'];

    public function getNombreSucursalAttribute(){
        return $this->sucursal()->pluck('nombre')->first();
    }

    public function getNombreProductoAttribute(){
        return $this->producto()->pluck('nombre')->first();
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal', 'sucursal_id');
    }

    // public function inventarios(){
    //     return $this->hasMany('App\Models\Inventario\Inventario', 'sucursal_id', 'sucursal_id');
    // }


}



