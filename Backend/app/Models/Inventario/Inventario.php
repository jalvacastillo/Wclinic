<?php

namespace App\Models\Inventario;

use Illuminate\Database\Eloquent\Model;

class Inventario extends Model {

    protected $table = 'producto_inventarios';
    protected $fillable = array(
        'producto_id',
        'stock',
        'stock_min',
        'stock_max',
        'nota',
        'bodega_id',
    );

    protected $appends = ['nombre_bodega', 'nombre_sucursal'];

    public function getNombreBodegaAttribute(){
        return $this->bodega()->pluck('nombre')->first();
    }

    public function getNombreSucursalAttribute(){
        return $this->bodega()->first()->nombre_sucursal;
    }

    public function producto(){
        return $this->belongsTo('App\Models\Inventario\Producto', 'producto_id');
    }

    public function bodega(){
        return $this->belongsTo('App\Models\Admin\Bodega', 'bodega_id');
    }


}



