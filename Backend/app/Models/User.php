<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'tipo',
        'telefono',
        'direccion',
        'dui',
        'nit',
        'nota',
        'avatar',
        'codigo',
        'activo',
        'empleado',
        'caja_id',
        'departamento_id',
        'sucursal_id',
        'ultimo_login',
        'ultimo_logout',
    ];

    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['empresa_id', 'caja_chica_id','nombre_empresa','nombre_sucursal', 'nombre_caja', 'ultimo_login_human', 'ultimo_logout_human'];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'activo' => 'boolean',
        'empleado' => 'boolean',
    ];

    public function getUltimoLoginHumanAttribute(){
        if ($this->ultimo_login) {
            return Carbon::parse($this->ultimo_login)->diffForhumans();
        }
    }

    public function getUltimoLogoutHumanAttribute(){
        if ($this->ultimo_logout) {
                return Carbon::parse($this->ultimo_logout)->diffForhumans();
        }
    }

    public function getNombreCajaAttribute(){
        return $this->caja()->pluck('nombre')->first();
    }

    public function getNombreSucursalAttribute(){
        return $this->sucursal()->pluck('nombre')->first();
    }

    public function getNombreEmpresaAttribute(){
        return $this->sucursal()->first()->empresa()->pluck('nombre')->first();
    }

    public function getEmpresaIDAttribute(){
        return $this->sucursal()->first()->empresa()->pluck('id')->first();
    }

    public function getCajaChicaIDAttribute(){
        return $this->cajachica()->pluck('id')->first();
    }

    public function sucursal(){
        return $this->belongsTo('App\Models\Admin\Sucursal', 'sucursal_id');
    }

    public function caja(){
        return $this->belongsTo('App\Models\Admin\Caja', 'caja_id');
    }

    public function cajachica(){
        return $this->hasOne('App\Models\Contabilidad\CajaChica\CajaChica', 'usuario_id');
    }

    public function cortes(){
        return $this->hasMany('App\Models\Admin\Corte', 'usuario_id');
    }

    public function ventas(){
        return $this->hasMany('App\Models\Ventas\Venta', 'usuario_id')->where('estado', 'Cobrada');
    }

    public function compras(){
        return $this->hasMany('App\Models\Compras\Compra', 'usuario_id');
    }

    public function getJWTIdentifier() {
      return $this->getKey();
    }

    public function getJWTCustomClaims() {
      return [];
    }

}
