<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model {

    protected $table = 'caja_documentos';
    protected $fillable = array(
        'nombre',
        'actual',
        'inicial',
        'final',
        'caja_id'

    );

    public function caja(){
        return $this->belongsTo('App\Models\Admin\Caja', 'caja_id');
    }


}



