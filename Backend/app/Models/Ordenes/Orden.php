<?php

namespace App\Models\Ordenes;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Orden extends Model {
    
    protected $table = 'ordenes';
    protected $fillable = [
        'fecha', 
        'estado',
        'tipo_servicio',
        'total',
        'cliente_id', 
        'usuario_id', 
        'sucursal_id'
    ];

    protected $appends = ['nombre_cliente', 'nombre_usuario', 'tiempo', 'created_at_human'];

    function getFechaAttribute($value)
    {
         return Carbon::parse($value)->format('Y-m-d');
    }

    function getTiempoAttribute($value)
    {
         return $this->detalles()->max('tiempo');
    }

    public function getCreatedAtHumanAttribute(){
        return Carbon::parse($this->created_at)->diffForhumans();
    }
 
    public function empresa() 
    {
        return $this->belongsTo('App\Models\Admin\Empresa', 'empresa_id');
    }

    public function getNombreUsuarioAttribute() 
    {
        return $this->usuario()->pluck('name')->first();
    }

    public function getNombreClienteAttribute() 
    {
        return $this->cliente()->pluck('nombre')->first();
    }

    public function scopeDelDia($query)
    {
        return $query->where('fecha', date('Y-m-d'));
    }

    public function scopeDelMes($query)
    {
        return $query->whereMonth('fecha', date('m'))->whereYear('fecha', date('Y'));
    }


    public function detalles() 
    {
        return $this->hasMany('App\Models\Ordenes\Detalle');
    }

    public function cliente() 
    {
        return $this->belongsTo('App\Models\Ventas\Clientes\Cliente', 'cliente_id');
    }

    public function usuario() 
    {
        return $this->belongsTo('App\Models\User', 'usuario_id');
    }


}
