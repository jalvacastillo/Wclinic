<?php

namespace App\Models\Pacientes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model {

    protected $table = 'paciente_documentos';
    protected $fillable = array(
        'fecha',
        'nombre',
        'url',
        'paciente_id',
        'usuario_id',
    );

    protected $appends = ['nombre_paciente'];

    public function getPacienteAttribute()
    {
        return $this->paciente()->pluck('nombre')->first();
    }

    public function paciente(){
        return $this->belongsTo('App\Models\Pacientes\Paciente', 'paciente_id');
    }



}

