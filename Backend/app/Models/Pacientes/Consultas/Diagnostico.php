<?php

namespace App\Models\Pacientes\Consultas;

use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model {

    protected $table = 'paciente_consultas_diagnosticos';
    protected $fillable = array(
        'codigo',
        'descripcion',
        'consulta_id'
    );


    public function consulta(){
        return $this->belongsTo('App\Models\Pacientes\Consultas\Consulta', 'consulta_id');
    }



}



