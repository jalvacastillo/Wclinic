<?php

namespace App\Models\Pacientes\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Heces extends Model {

    use SoftDeletes;
    protected $table = 'examenes_heces';
    protected $fillable = array(
        'color',
        'consistencia',
        'sangre',
        'restos',
        'entrocitos',
        'levadura',
        'mucus',
        'leucocitos',
        'flora',
        // 'protozoarios',
        'quistes',
        'larvas',
        // 'metazueros',
        'observaciones',
        'sucursal_id'
    );


   

    public function examenes(){
        return $this->belongsTo('App\Models\Pacientes\Examenes', 'examen_id');
    }
}



