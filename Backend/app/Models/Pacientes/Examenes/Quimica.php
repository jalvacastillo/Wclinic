<?php

namespace App\Models\Pacientes\Examenes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quimica extends Model {

    protected $table = 'examenes_quimicas';
    protected $fillable = array(
        'observaciones',
        'sucursal_id'
    );

    protected $appends = ['resultados'];

    public function getResultadosAttribute(){
        return $this->resultados();
    }


    public function examenes(){
        return $this->belongsTo('App\Models\Pacientes\Examenes', 'examen_id');
    }

    public function resultados(){
        return $this->hasMany('App\Models\Pacientes\Examenes\QuimicaDetalle', 'quimica_id');
    }
}

