<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenesDiversosTable extends Migration {

	public function up()
	{
		Schema::create('examenes_diversos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('muestra')->nullable();
			$table->string('examen')->nullable();
			$table->string('tipo')->nullable();
			$table->text('resultado')->nullable();

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('examenes_diversos');
	}

}
