<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamenesTable extends Migration {

    public function up()
    {
        Schema::create('examenes', function(Blueprint $table)
        {
            $table->increments('id');
            
            $table->string('especialidad');
            $table->text('nombre');
            $table->string('descripcion')->nullable();
            $table->string('etiquetas')->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('examenes');
    }

}
