<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoImagenesTable extends Migration {

    public function up()
    {
        Schema::create('producto_imagenes', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('img');
            $table->integer('producto_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('producto_imagenes');
    }

}
