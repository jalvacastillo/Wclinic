<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosAsistenciasTable extends Migration
{

    public function up()
    {
        Schema::create('empleados_asistencias', function (Blueprint $table) {
            $table->increments('id');
            
            $table->datetime('entrada')->nullable();
            $table->datetime('salida')->nullable();
            $table->string('ubicacion')->nullable();
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('empleados_asistencias');
    }
}
