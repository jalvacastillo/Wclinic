<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaSucursalGastosTable extends Migration {

	public function up()
	{
		Schema::create('empresa_sucursal_gastos', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->string('descripcion');
			$table->string('categoria');
			$table->decimal('total', 9, 2);
			$table->integer('usuario_id');
			$table->integer('sucursal_id');
			$table->timestamps();

		});
	}

	public function down()
	{
		Schema::drop('empresa_sucursal_gastos');
	}

}
