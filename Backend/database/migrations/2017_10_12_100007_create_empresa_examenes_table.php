<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaExamenesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('empresa_examenes', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('nombre',100)->nullable();
			$table->string('especialidad',100)->nullable();
			$table->string('descripcion',100)->nullable();
			$table->string('etiquetas',100)->nullable();
			$table->integer('empresa_id');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('empresa_examenes');
	}

}
