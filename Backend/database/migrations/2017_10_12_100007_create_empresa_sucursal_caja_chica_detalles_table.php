<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaSucursalCajaChicaDetallesTable extends Migration {

    public function up()
    {
        Schema::create('empresa_sucursal_caja_chica_detalles', function(Blueprint $table)
        {
            $table->increments('id');

            $table->date('fecha');
            $table->string('descripcion')->nullable();
            $table->string('referencia')->nullable();
            $table->string('tipo');
            $table->decimal('entrada', 9,2)->nullable();
            $table->decimal('salida', 9,2)->nullable();
            $table->decimal('saldo', 9,2)->nullable();
            $table->integer('usuario_id');
            $table->integer('caja_id');
            $table->timestamps();

        });
    }

    public function down()
    {
        Schema::drop('empresa_sucursal_caja_chica_detalles');
    }

}
