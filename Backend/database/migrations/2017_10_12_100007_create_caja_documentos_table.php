<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCajaDocumentosTable extends Migration {

    public function up()
    {
        Schema::create('caja_documentos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('nombre');
            $table->integer('actual');
            $table->integer('inicial');
            $table->integer('final');
            $table->integer('caja_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('caja_documentos');
    }

}
