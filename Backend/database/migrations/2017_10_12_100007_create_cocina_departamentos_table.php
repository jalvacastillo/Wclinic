<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCocinaDepartamentosTable extends Migration
{

    public function up()
    {
        Schema::create('cocina_departamentos',function($table) {
            $table->increments('id');

            $table->string('nombre');
            $table->integer('sucursal_id')->unsigned();
            
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('cocina_departamentos');
    }
}
