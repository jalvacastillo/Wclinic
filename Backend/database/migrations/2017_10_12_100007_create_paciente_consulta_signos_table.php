<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacienteConsultaSignosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente_consulta_signos', function(Blueprint $table)
        {
            $table->increments('id');

            $table->datetime('fecha');
            $table->decimal('peso',6,2);
            $table->string('peso_unidad');
            $table->decimal('estatura',6,2);
            $table->string('estatura_unidad');
            $table->decimal('temperatura',6,2);
            $table->string('temperatura_unidad');
            $table->string('presion')->nullable();
            $table->string('nota')->nullable();
            $table->integer('consulta_id');
            $table->integer('usuario_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paciente_consulta_signos');
    }

}
