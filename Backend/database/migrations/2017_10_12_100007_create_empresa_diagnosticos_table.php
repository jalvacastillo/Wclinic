<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaDiagnosticosTable extends Migration {

    public function up()
    {
        Schema::create('empresa_diagnosticos', function(Blueprint $table)
        {
            $table->increments('id');
            
            $table->string('codigo');
            $table->text('descripcion');
            $table->text('equivalencias')->nullable();
            $table->integer('empresa_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_diagnosticos');
    }

}
