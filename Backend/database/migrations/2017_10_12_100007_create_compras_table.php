<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasTable extends Migration {

	public function up()
	{
		Schema::create('compras', function(Blueprint $table)
		{
			$table->increments('id');

			$table->date('fecha');
			$table->string('estado');
			$table->string('tipo');
			$table->string('referencia')->nullable();
			$table->integer('proveedor_id');
			$table->date('fecha_pago');			
			$table->decimal('iva_retenido', 9,4);
			$table->decimal('descuento', 9,2);
			$table->decimal('iva', 9,2);
			$table->decimal('subtotal', 9,2);
			$table->decimal('total', 9,2);
			$table->integer('usuario_id');
			$table->integer('empresa_id');

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('compras');
	}

}
