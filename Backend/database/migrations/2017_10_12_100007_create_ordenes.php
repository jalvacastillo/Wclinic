<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenes extends Migration
{

    public function up()
    {
        Schema::create('ordenes',function($table) {
            $table->increments('id');

            $table->date('fecha');
            $table->string('estado');
            $table->string('tipo_servicio')->default('Sala de Venta');
            $table->decimal('total', 6,2);
            $table->integer('cliente_id');
            $table->integer('usuario_id');
            $table->integer('sucursal_id');
            
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::drop('ordenes');
    }
}
