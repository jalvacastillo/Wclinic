<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventoDetallesTable extends Migration {

	public function up()
	{
        Schema::create('evento_detalles',function($table) {
            $table->increments('id');

            $table->string('servicio');
            $table->integer('cantidad');
            $table->decimal('precio',6,2);
            $table->decimal('total', 6,2);
            $table->text('nota')->nullable();
            $table->integer('evento_id')->unsigned();
            
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('evento_detalles');
	}

}
