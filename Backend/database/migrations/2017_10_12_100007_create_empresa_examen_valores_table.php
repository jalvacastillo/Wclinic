<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresaExamenValoresTable extends Migration {

	public function up()
	{
		Schema::create('empresa_examen_valores', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('examen_id');
			$table->string('valor',250);
			
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('empresa_examen_valores');
	}

}
