    <?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasPlanillaDetallesTable extends Migration {

    public function up()
    {
        Schema::create('empresa_planilla_detalles', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('empleado_id');
            $table->integer('dias')->default(0);
            $table->integer('horas')->default(0);
            $table->integer('horas_extras')->default(0);
            $table->decimal('sueldo',6,2)->default(0);
            $table->decimal('extras',6,2)->default(0);
            $table->decimal('otros',6,2)->default(0);
            $table->decimal('renta',6,2)->default(0);
            $table->decimal('isss',6,2)->default(0);
            $table->decimal('afp',6,2)->default(0);
            $table->decimal('total',6,2)->default(0);
            $table->integer('planilla_id');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('empresa_planilla_detalles');
    }

}
