<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            EmpresaTableSeeder::class,
            UsersTableSeeder::class,
            CajasTableSeeder::class,
            BodegasTableSeeder::class,
            ClientesTableSeeder::class,
            CategoriasTableSeeder::class,
            ProductosTableSeeder::class,
            MateriasPrimasTableSeeder::class,

            OrdenesTableSeeder::class,
            VentasTableSeeder::class,
            ComprasTableSeeder::class,
            GastosTableSeeder::class,
            ProveedoresTableSeeder::class,

            PacientesTableSeeder::class,
            ConsultasTableSeeder::class,
            ExamenesTableSeeder::class,
            DataTableSeeder::class,
            CitasTableSeeder::class,

        ]);
    }
}
        
