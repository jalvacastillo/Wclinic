<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pacientes\Cita;
     
class CitasTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 50 ; $i++)
        {
                $table = new Cita;

                $table->fecha           = $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now', $timezone = null);
                $table->paciente_id     = $faker->numberBetween(1,50);
                $table->tipo            = $faker->randomElement(['Urgencia', 'Consulta']);
                $table->estado          = $faker->randomElement(['Agendada', 'En Proceso', 'Finalizada', 'Cancelada']);
                $table->frecuencia      = '';
                $table->nota            = $faker->text;
                $table->medico_id       = 1;
                $table->usuario_id      = 1;

                $table->save();
            
        }


    }
     
}
