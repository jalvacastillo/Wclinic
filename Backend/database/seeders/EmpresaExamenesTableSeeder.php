<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pacientes\Consultas\Examenes\Examen;
use App\Models\Pacientes\Consultas\Examenes\ExamenValor;

use App\Models\Pacientes\Examenes\Heces;
use App\Models\Pacientes\Examenes\Hemograma;
use App\Models\Pacientes\Examenes\Orina;
use App\Models\Pacientes\Examenes\Quimica;
use App\Models\Pacientes\Examenes\QuimicaDetalle;
use App\Models\Pacientes\Examenes\Diverso;

     
class EmpresaExamenesTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        // Examenes

            $ex =  array(
                "Glucosa",
                "Glucosa pos pandrial",
                "Triglicéridos",
                "Colesterol",
                "Colesterol HDL",
                "Colesterol LDL",
                "Creatinina",
                "Acido Úrico",
                "Nitrógeno Ureico",
                "Urea",
                "TGP / ALT",
                "TGO / AST",
                "Proteínas totales",
                "Albumina",
                "Globulina",
                "Relación  A/G",
                "Bilirrubina total",
                "Bilirrubina directa",
                "Creatinina fosfoquinasa",
                "Fosfatasa alcalina",
                "Fosfatasa alcalina ( Barry )",
                "Proteína en orina 24/h",
                "Sodio en sangre",
                "Calcio en sangre",
                "Potasio en sangre",
                "Fosforo en sangre",
                "Cloro en sangre",
                "Fosfatasa acida",
                "Hemoglobina glicosilada",
                "Potasio en sangre – barry",
                "Proteína en orina 24/h",
                "Magnesio",
                "Tolerancia a la glucosa  5/h ",
                "Amilasa",
                "Lipasa",
                "PSA",
                "Tiempo de protrombina  TP",
                "T.de tromboplastina parcial TTp",
                "Tiempo de trombina TT",
                "Velocidad de eritrocediment",
                "Tiempo de sangramiento",
                "Tiempo de coagulación",
                "T3, ( Triyodotironina )",
                "T4, ( Tiroxina )",
                "TSH",
                "Estreptolicina o ( ASO )- ultra)",
                "Estreptolicina o ( ASO ) barry",
                "Alfa feto proteína ( AFP )",
                "( En embarazó )",
                "Proteína c reactiva (PCR) ultra",
                "Proteína C reactiva (PCR) Barry",
                "Factor reumatoideo",
                "Microalbumina en orina",
                "Anticuerpos para dengue IgM",
                "Anticuerpos para dengue IgG",
                "Antígenos febriles",
                "Ca 19 – 9 Elisa ( Cuantitativa )",
                "Toxoplasmosis IgG ( barry )",
                "Toxoplasmosis IgM (barry )",
                "Gama glutamil transferasa (GG)",
                "Factor reumatoideo ( FR )",
                "Prolactina",
                "Insulina en ayunas",
                "Anticoagulante lupico ",
                "Cardiolipina  IgG",
                "Cardiolipina  IgM",
                "Fibrinogeno",
                "Concentracion de crea",
                "Depuracion de crea 24 /h",
                "IgE, Total",
                "Ac. Antinucleares latex  ( ANA )",
                "Ac. Anticitrolinados IgG",
                "Helicobacter pylori IgM, IgG.",
                "Cortisol,  AM",
                "Microalbumina ( Orina )",
                "Hierro  serico",
                "Alfafetoproteinas",
                "Gonadotropina corionica cuant.",
                "Peptido Natriuretico ( BNP )",
                "Hb.Electroforesis"
               );


            for ($i=0; $i < count($ex); $i++) { 
                $examen = new Examen;
                $examen->nombre     = $ex[$i];
                $examen->empresa_id = 1;
                $examen->save();
            }

            // Valores

                ExamenValor::create(array( 'examen_id' =>  '1', 'valor' => '70 – 105 mg /dl'));
                ExamenValor::create(array( 'examen_id' =>  '2', 'valor' => 'Hasta 140 mg /dl '));
                ExamenValor::create(array( 'examen_id' =>  '3', 'valor' => 'Hasta 150 mg /dl '));
                ExamenValor::create(array( 'examen_id' =>  '4', 'valor' => 'Hasta 200 mg /dl'));
                ExamenValor::create(array( 'examen_id' =>  '5', 'valor' => 'No riesgo mayor de 60 mg /dl  '));
                ExamenValor::create(array( 'examen_id' =>  '5', 'valor' => 'Riesgo menor de 35 mg /dl '));
                ExamenValor::create(array( 'examen_id' =>  '6', 'valor' => 'Menor de 150 mg /dl '));
                ExamenValor::create(array( 'examen_id' =>  '7', 'valor' => 'Hombres 0.7  - 1.3 mg /dl'));
                ExamenValor::create(array( 'examen_id' =>  '7', 'valor' => 'Mujeres 0.6 – 1.1'));
                ExamenValor::create(array( 'examen_id' =>  '8', 'valor' => '2.4 – 7.0 mg /dl '));
                ExamenValor::create(array( 'examen_id' =>  '9', 'valor' => '8 – 23 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '10', 'valor' => '17 – 49 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '11', 'valor' => 'Hombres menor de 40 UI'));
                ExamenValor::create(array( 'examen_id' => '11', 'valor' => 'Mujeres menor de 32 UI'));
                ExamenValor::create(array( 'examen_id' => '12', 'valor' => '8 – 33 UI'));
                ExamenValor::create(array( 'examen_id' => '13', 'valor' => '5.8 – 8.8 /dl'));
                ExamenValor::create(array( 'examen_id' => '13', 'valor' => 'Neonatos '));
                ExamenValor::create(array( 'examen_id' => '13', 'valor' => 'Hasta 1 semana 4.4 – 7.14 6  g/dl'));
                ExamenValor::create(array( 'examen_id' => '14', 'valor' => '3.4 – 5.0  g /dl'));
                ExamenValor::create(array( 'examen_id' => '15', 'valor' => '2.6 – 3.1 g /dl'));
                ExamenValor::create(array( 'examen_id' => '16', 'valor' => '1.1 – 3.1 g /dl '));
                ExamenValor::create(array( 'examen_id' => '17', 'valor' => 'Hasta 1.20'));
                ExamenValor::create(array( 'examen_id' => '18', 'valor' => 'Hasta  0.50'));
                ExamenValor::create(array( 'examen_id' => '19', 'valor' => 'Hasta 170 U / L'));
                ExamenValor::create(array( 'examen_id' => '20', 'valor' => '40 – 129 U/L'));
                ExamenValor::create(array( 'examen_id' => '21', 'valor' => 'Hombres < - 270 U/I'));
                ExamenValor::create(array( 'examen_id' => '21', 'valor' => 'Mujeres < - 240 U/I'));
                ExamenValor::create(array( 'examen_id' => '22', 'valor' => 'Hombres <  5.4 – U/L'));
                ExamenValor::create(array( 'examen_id' => '22', 'valor' => 'Mujeres <  4.2 – U/L'));
                ExamenValor::create(array( 'examen_id' => '23', 'valor' => '10 – 150 mg / 24 horas'));
                ExamenValor::create(array( 'examen_id' => '24', 'valor' => 'General  135 – 155 mmol /L'));
                ExamenValor::create(array( 'examen_id' => '25', 'valor' => 'Recién nacido 8 – 13 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '25', 'valor' => 'Niño 10 – 12 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '25', 'valor' => 'Adulto  8.5 – 10.5 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '26', 'valor' => '3.6 – 5.5 mmol /L'));
                ExamenValor::create(array( 'examen_id' => '27', 'valor' => 'Adultos 2.5 – 5.0 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '27', 'valor' => 'Neonatos 5  - 9.6 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '27', 'valor' => 'Niños hasta 1 año  5 – 10 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '27', 'valor' => 'Mayor de 60 años 3.4 – 6.2 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '28', 'valor' => '95 – 115 mmol/L'));
                ExamenValor::create(array( 'examen_id' => '29', 'valor' => 'Paciente  no diabético 4.2 - 6.2%'));
                ExamenValor::create(array( 'examen_id' => '29', 'valor' => 'Buen control 5.5 – 6.8 %'));
                ExamenValor::create(array( 'examen_id' => '29', 'valor' => 'Control regular 6.8 – 7.6 %'));
                ExamenValor::create(array( 'examen_id' => '29', 'valor' => 'Control pobre mayor de > -7.6 %'));
                ExamenValor::create(array( 'examen_id' => '30', 'valor' => 'Suero 3.6 – 5.5 mmol/L'));
                ExamenValor::create(array( 'examen_id' => '30', 'valor' => 'Plasma 4.0 – 4.8 mmoI/L'));
                ExamenValor::create(array( 'examen_id' => '31', 'valor' => 'Menor de 100 mg /24horas'));
                ExamenValor::create(array( 'examen_id' => '31', 'valor' => 'M. embarazadas menor de 150 mg/24 h.'));
                ExamenValor::create(array( 'examen_id' => '32', 'valor' => 'General 1.6 – 2.5 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '33', 'valor' => 'En ayunas  70 – 105 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '33', 'valor' => '1- menor de 190 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '33', 'valor' => '2- menor de 160 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '33', 'valor' => '3- menor de 140 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '33', 'valor' => '4- menor de 120 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '34', 'valor' => 'Suero 25 – 125 U/L'));
                ExamenValor::create(array( 'examen_id' => '34', 'valor' => 'Orina 1 – 17 U/L'));
                ExamenValor::create(array( 'examen_id' => '35', 'valor' => 'Menor o igual  - 66 U/L '));
                ExamenValor::create(array( 'examen_id' => '36', 'valor' => 'Normal o bajo riesgo 0 - 4 ng/mL'));
                ExamenValor::create(array( 'examen_id' => '36', 'valor' => 'Mediano riesgo 4-10 ng/mL'));
                ExamenValor::create(array( 'examen_id' => '36', 'valor' => 'Alto riesgo mayor. 10 ng/mL'));
                ExamenValor::create(array( 'examen_id' => '37', 'valor' => '10 – 14 Segundos ( 10 – 16 segundos )'));
                ExamenValor::create(array( 'examen_id' => '38', 'valor' => '20 – 35 Segundos ( 20 – 35 segundos )'));
                ExamenValor::create(array( 'examen_id' => '39', 'valor' => '16  - 23 segundos '));

                ExamenValor::create(array( 'examen_id' => '40', 'valor' => 'Mujeres 0 – 15 mm /h'));
                ExamenValor::create(array( 'examen_id' => '40', 'valor' => 'Hombres 0 – 7 mm /h'));
                ExamenValor::create(array( 'examen_id' => '41', 'valor' => '1 – 4 minutos'));
                ExamenValor::create(array( 'examen_id' => '42', 'valor' => '5 – 10 minutos'));
                ExamenValor::create(array( 'examen_id' => '43', 'valor' => '0.8 – 2.0 ng /ml'));
                ExamenValor::create(array( 'examen_id' => '44', 'valor' => '5.0 – 13.0 ug /dl'));
                ExamenValor::create(array( 'examen_id' => '45', 'valor' => '0.4 – 7.0  uUI /ml'));
                ExamenValor::create(array( 'examen_id' => '46', 'valor' => 'Menor o Igual a 200 UI/L'));
                ExamenValor::create(array( 'examen_id' => '47', 'valor' => 'Negativo menor de 200 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '47', 'valor' => 'Positivo mayor o igual 200 mg/dl'));
                ExamenValor::create(array( 'examen_id' => '48', 'valor' => '0.5 – 5.8 UI /mL'));
                ExamenValor::create(array( 'examen_id' => '49', 'valor' => '14 semana  23.2'));
                ExamenValor::create(array( 'examen_id' => '49', 'valor' => '15 semana  25.6 '));
                ExamenValor::create(array( 'examen_id' => '49', 'valor' => '17 semana  33.5'));
                ExamenValor::create(array( 'examen_id' => '49', 'valor' => '18 semana  40.1'));
                ExamenValor::create(array( 'examen_id' => '49', 'valor' => '19 semana  45.4'));
                ExamenValor::create(array( 'examen_id' => '50', 'valor' => 'Menor o igual  6 - 0 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '51', 'valor' => '0.02 – 1.35 mg /dl'));
                ExamenValor::create(array( 'examen_id' => '52', 'valor' => 'Hasta 200 UI / ml'));
                ExamenValor::create(array( 'examen_id' => '53', 'valor' => '0.8 – 11.2 mg /dl '));
                ExamenValor::create(array( 'examen_id' => '54', 'valor' => 'No reactivo: 0.0 – 0.300'));
                ExamenValor::create(array( 'examen_id' => '54', 'valor' => 'Débil reactivo: 0.5 <- 1.0 '));
                ExamenValor::create(array( 'examen_id' => '54', 'valor' => 'Reactivo: mayor de 1.0'));
                ExamenValor::create(array( 'examen_id' => '55', 'valor' => 'No reactivo: 0.0 – 0.300'));
                ExamenValor::create(array( 'examen_id' => '55', 'valor' => 'Débil reactivo: 0.5 <- 1.0'));
                ExamenValor::create(array( 'examen_id' => '55', 'valor' => 'Reactivo: mayor de 1.0'));
                ExamenValor::create(array( 'examen_id' => '56', 'valor' => 'Salmonella  typhi   O'));
                ExamenValor::create(array( 'examen_id' => '56', 'valor' => 'Salmonella  typhi   H'));
                ExamenValor::create(array( 'examen_id' => '56', 'valor' => 'Salmonella  paratyphi   a'));
                ExamenValor::create(array( 'examen_id' => '56', 'valor' => 'Salmonella  paratyphi   b'));
                ExamenValor::create(array( 'examen_id' => '56', 'valor' => 'Brucella  abortus'));
                ExamenValor::create(array( 'examen_id' => '56', 'valor' => 'Proteus  O X 19 '));
                ExamenValor::create(array( 'examen_id' => '57', 'valor' => 'Hasta 35.0 – U/ml'));
                ExamenValor::create(array( 'examen_id' => '58', 'valor' => 'Negativo < - 0.90 UI /ml'));
                ExamenValor::create(array( 'examen_id' => '58', 'valor' => 'Positivo > - 1.00 UI/ml'));
                ExamenValor::create(array( 'examen_id' => '59', 'valor' => 'Negativo < - 0.90 '));
                ExamenValor::create(array( 'examen_id' => '59', 'valor' => 'Positivo > - 1.00 '));
                ExamenValor::create(array( 'examen_id' => '60', 'valor' => 'Mujeres:  hasta  39 U/L'));

                ExamenValor::create(array( 'examen_id' => '60', 'valor' => 'Hombres: hasta  66 U/L'));
                ExamenValor::create(array( 'examen_id' => '61', 'valor' => 'Menor o igual 12 U/mL'));
                ExamenValor::create(array( 'examen_id' => '62', 'valor' => 'Mujeres 1 – 25 ng /ml'));
                ExamenValor::create(array( 'examen_id' => '62', 'valor' => 'Hombres 1 – 20 ng /ml '));
                ExamenValor::create(array( 'examen_id' => '62', 'valor' => '3º trimestre de embarazo 95.0 – 473ng/dl'));
                ExamenValor::create(array( 'examen_id' => '63', 'valor' => 'Niños menores  12 años menor de 10uIu/ml'));
                ExamenValor::create(array( 'examen_id' => '63', 'valor' => 'Adultos  7 – 9.0uIu /ml'));
                ExamenValor::create(array( 'examen_id' => '63', 'valor' => 'Paciente diabético   7 – 25uIu /ml'));
                ExamenValor::create(array( 'examen_id' => '64', 'valor' => 'Negativo '));
                ExamenValor::create(array( 'examen_id' => '65', 'valor' => 'Negativo  Menor de 10 GPL'));
                ExamenValor::create(array( 'examen_id' => '66', 'valor' => 'Negativo  Menor de  5.0 MPL'));
                ExamenValor::create(array( 'examen_id' => '67', 'valor' => '2.0 – 4.0 g /L'));
                ExamenValor::create(array( 'examen_id' => '68', 'valor' => '600 – 2000 ml /minutos'));
                ExamenValor::create(array( 'examen_id' => '69', 'valor' => 'Mujeres 88 – 128 ml /minutos '));
                ExamenValor::create(array( 'examen_id' => '69', 'valor' => 'Hombres  97 – 137 ml /minutos '));
                ExamenValor::create(array( 'examen_id' => '70', 'valor' => 'Hasta 150 I  u /mL'));
                ExamenValor::create(array( 'examen_id' => '71', 'valor' => 'Negativa'));
                ExamenValor::create(array( 'examen_id' => '72', 'valor' => 'Negativo  Menor  a 17 U /mL'));
                ExamenValor::create(array( 'examen_id' => '72', 'valor' => 'Positivo Mayor o igual a 17 U /mL'));
                ExamenValor::create(array( 'examen_id' => '73', 'valor' => 'Negativo menor de 0.90'));
                ExamenValor::create(array( 'examen_id' => '73', 'valor' => 'Positivo mayor de 1.0'));
                ExamenValor::create(array( 'examen_id' => '74', 'valor' => 'Adultos 5 – 23  ug /dL'));
                ExamenValor::create(array( 'examen_id' => '74', 'valor' => 'Niño 3 – 21 ug /dL'));
                ExamenValor::create(array( 'examen_id' => '75', 'valor' => 'Adultos menor de 20 mg /L'));
                ExamenValor::create(array( 'examen_id' => '75', 'valor' => 'Niños de 3 a 5 años   menor de 20 mg /L'));
                ExamenValor::create(array( 'examen_id' => '76', 'valor' => '50 – 170 ug /dL'));
                ExamenValor::create(array( 'examen_id' => '77', 'valor' => 'Hasta 8 – 5 ng /mL'));
                ExamenValor::create(array( 'examen_id' => '78', 'valor' => '0 – 5 mIU /mL'));
                ExamenValor::create(array( 'examen_id' => '78', 'valor' => '1ª  Semana 5 – 50 mIU /mL'));
                ExamenValor::create(array( 'examen_id' => '78', 'valor' => '10ª  Semana  100,000 – 200,000  mIU /mL'));
                ExamenValor::create(array( 'examen_id' => '79', 'valor' => 'Menor de 125 pg /mL'));
                ExamenValor::create(array( 'examen_id' => '80', 'valor' => 'Hb A2-  0.00 – 3.50'));
                ExamenValor::create(array( 'examen_id' => '80', 'valor' => 'Hb F - 0.00 – 1.00'));
                ExamenValor::create(array( 'examen_id' => '80', 'valor' => 'Hb A – 96.00  - 99.0'));

        // Heces
            for($i = 1; $i <= 30 ; $i++)
            {
                $table = new Heces;

                $table->color = "Lorem ipsum.";
                $table->consistencia = "Lorem ipsum.";
                $table->sangre = "Lorem ipsum.";
                $table->restos = "Lorem ipsum.";
                $table->entrocitos = "Lorem ipsum.";
                $table->levadura = "Lorem ipsum.";
                $table->mucus = "Lorem ipsum.";
                $table->leucocitos = "Lorem ipsum.";
                $table->flora = "Lorem ipsum.";
                // $table->protozoarios = "Lorem ipsum.";
                $table->quistes = "Lorem ipsum.";
                $table->larvas = "Lorem ipsum.";
                // $table->metazueros = "Lorem ipsum.";
                $table->observaciones = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, modi!";
                
                $table->save();
                
            }

        // Hemograma
            for($i = 1; $i <= 30 ; $i++)
            {
                $table = new Hemograma;

                $table->globulos = $faker->numberBetween(1,5);
                $table->hematocritos = $faker->numberBetween(1,5);
                $table->hemoglobina = $faker->numberBetween(1,5);
                $table->volumen = $faker->numberBetween(1,5);
                $table->concentracion = $faker->numberBetween(1,5);
                $table->globular = $faker->numberBetween(1,5);
                $table->blancos = $faker->numberBetween(1,5);
                $table->basofitos = $faker->numberBetween(1,5);
                $table->eosinofilos = $faker->numberBetween(1,5);
                $table->neutrofilos = $faker->numberBetween(1,5);
                $table->linfocitos = $faker->numberBetween(1,5);
                $table->monocitos = $faker->numberBetween(1,5);
                $table->plaquetas = $faker->numberBetween(1,5);
                $table->observaciones = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, modi!";
                
                $table->save();
                
            }

        // Orina
            for($i = 1; $i <= 30 ; $i++)
            {
                $table = new Orina;

                $table->color = 'Lorem ipsum.';
                $table->aspecto = 'Lorem ipsum.';
                $table->densidad = 'Lorem ipsum.';
                $table->esterasa = 'Lorem ipsum.';
                $table->nitritos = 'Lorem ipsum.';
                $table->reaccion = 'Lorem ipsum.';
                $table->proteinas = 'Lorem ipsum.';
                $table->glucosa = 'Lorem ipsum.';
                $table->cetonicos = 'Lorem ipsum.';
                $table->urobitmogeno = 'Lorem ipsum.';
                $table->bilirubina = 'Lorem ipsum.';
                $table->sangre = 'Lorem ipsum.';
                $table->bacterias = 'Lorem ipsum.';
                $table->leucocitos = 'Lorem ipsum.';
                $table->hematies = 'Lorem ipsum.';
                $table->cilindros = 'Lorem ipsum.';
                $table->cristales = 'Lorem ipsum.';
                $table->celulas = 'Lorem ipsum.';
                $table->otros = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio, modi!';
                
                $table->save();
                
            }

        // Quimica
            for($i = 1; $i <= 30 ; $i++)
            {
                $table = new Quimica;

                $table->observaciones = "Lorem ipsum dolor sit.";
                $table->save();
                
            }

            for($i = 1; $i <= 30 ; $i++)
            {
                $quimicaResultado = new QuimicaDetalle;

                $quimicaResultado->resultado = "Lorem ipsum dolor sit.";
                $quimicaResultado->examen = "Lorem ipsum.";
                $quimicaResultado->valor = "Lorem ipsum.";
                $quimicaResultado->quimica_id = $faker->numberBetween(1,30);
                $quimicaResultado->save();
                
            }

        // Diversos
            for($i = 1; $i <= 30 ; $i++)
            {
                $table = new Diverso;

                $table->muestra = "Lorem ipsum dolor sit.";
                $table->examen = "Lorem ipsum.";
                $table->tipo = $faker->numberBetween(1,5);
                $table->resultado = "Lorem ipsum.";
                
                $table->save();
                
            }

    }
     
}
