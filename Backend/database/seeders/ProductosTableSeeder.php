<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Inventario\Producto;
use App\Models\Inventario\Sucursal;
use App\Models\Inventario\Categorias\SubCategoria;
use App\Models\Inventario\Categorias\Categoria;
use App\Models\Inventario\Inventario;
use App\Models\Inventario\Composicion;
use App\Models\Inventario\Ajuste;
     
class ProductosTableSeeder extends Seeder {

    public function run()
    {

        $faker = \Faker\Factory::create();

        $SubCategorias = SubCategoria::count();

        for($i = 0; $i < 50 ; $i++)
        {
            $table = new Producto;

            $table->nombre          = $faker->name;
            $table->descripcion     = $faker->text(200);
            $table->codigo          = $faker->numberBetween(1111111111, 999999999);
            $table->medida          = $faker->randomElement(['Unidad']);
            $table->costo           = $faker->numberBetween(1, 30);
            $table->costo_anterior  = $table->costo;
            $table->precio          = $faker->numberBetween(1, 30);
            $table->tipo            = "Producto";
            $table->tipo_impuesto   = "Gravada";
            $table->empresa_id      = 1;
           
            $categoria = SubCategoria::where('id', $faker->numberBetween(1, $SubCategorias))->first();

            if ($categoria) {
                $table->categoria_id    = $categoria->id;
            }
           

            $table->save();
            
        }

        $table = new Producto;
        $table->nombre          = 'Teleconsulta';
        $table->medida          = $faker->randomElement(['Unidad']);
        $table->costo           = 0;
        $table->costo_anterior  = $table->costo;
        $table->precio          = 20;
        $table->tipo            = "Servicio";
        $table->tipo_impuesto   = "Gravada";
        $table->empresa_id      = 1;
        $table->categoria_id    = 1;
        $table->save();

        $table = new Producto;
        $table->nombre          = 'Consulta Subsecuente';
        $table->medida          = $faker->randomElement(['Unidad']);
        $table->costo           = 0;
        $table->costo_anterior  = $table->costo;
        $table->precio          = 10;
        $table->tipo            = "Servicio";
        $table->tipo_impuesto   = "Gravada";
        $table->empresa_id      = 1;
        $table->categoria_id    = 1;
        $table->save();

        $table = new Producto;
        $table->nombre          = 'Consulta de Primera Vez';
        $table->medida          = $faker->randomElement(['Unidad']);
        $table->costo           = 0;
        $table->costo_anterior  = $table->costo;
        $table->precio          = 5;
        $table->tipo            = "Servicio";
        $table->tipo_impuesto   = "Gravada";
        $table->empresa_id      = 1;
        $table->categoria_id    = 1;
        $table->save();

        $table = new Producto;
        $table->nombre          = 'Consulta General';
        $table->medida          = $faker->randomElement(['Unidad']);
        $table->costo           = 0;
        $table->costo_anterior  = $table->costo;
        $table->precio          = 5;
        $table->tipo            = "Servicio";
        $table->tipo_impuesto   = "Gravada";
        $table->empresa_id      = 1;
        $table->categoria_id    = 1;
        $table->save();


    }

     
}
