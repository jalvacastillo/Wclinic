<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Registros\Proveedor;
     
class ProveedoresTableSeeder extends Seeder {
     
    public function run()
    {
        $faker = \Faker\Factory::create();

        // $proveedores = [
        //     [ 'nombre' => 'INVERSIONES VIDA, S.A. DE C.V.'],
        //     [ 'nombre' => 'COMERCIALIZADORA INTERAMERICANA, S.A. DE C.V.'],
        //     [ 'nombre' => 'EMBOTELLADORA LA CASCADA, S.A.'],
        //     [ 'nombre' => 'LA CONSTANCIA. LTDA DE C.V.'],
        //     [ 'nombre' => 'E.D.T. EL SALVADOR, S.A. DE C.V.'],
        //     [ 'nombre' => 'PRICESMART EL SALVADOR, S.A. DE C.V.'],
        //     [ 'nombre' => 'STEINER, S.A. DE C.V.'],
        //     [ 'nombre' => 'DISTRIBUIDORA SALVADOREÑA, S.A. DE C.V.'],
        //     [ 'nombre' => 'POZUELO DE EL SALVADOR, S.A. DE C.V.'],
        //     [ 'nombre' => 'FRITO LAY, S.A. DE C.V.'],
        //     [ 'nombre' => 'PDC COMERCIAL EL SALVADOR, S.A. DE C.V.'],
        //     [ 'nombre' => 'C. IMBERTON, S.A. DE C.V.'],
        //     [ 'nombre' => 'GENERICO'],
        //     [ 'nombre' => 'SAVONA, S.A. DE C.V.'],
        //     [ 'nombre' => 'ESCARRSA, DE C.V.'],
        //     [ 'nombre' => 'BIMBO DE EL SALVADOR, S.A. DE C.V.'],
        //     [ 'nombre' => 'PRODUCTOS ALIMENTICIOS DIANA, S.A. DE C.V.'],
        //     [ 'nombre' => 'DISMO, S.A. DE C.V.'],
        //     [ 'nombre' => 'MEDRANO FLORES, S.A. DE C.V.'],
        //     [ 'nombre' => 'INVERSIONES LA FUENTE, S.A. DE C.V.'],
        //     [ 'nombre' => 'COMERCIALIZADORA REAL, S.A. DE C.V.'],
        //     [ 'nombre' => 'BENEDETTI ZELAYA, S.A. DE C.V.'],
        //     [ 'nombre' => 'GRUPO ENTU-SIASMO, S.A. DE C.V.'],
        //     [ 'nombre' => 'GUSTAVO JOSE ACEVEDO BERGANZA'],

        // ];
        
        // for($i = 0; $i < count($proveedores) ; $i++)
        for($i = 0; $i < 50 ; $i++)
        {
            $table = new Proveedor;

            $table->nombre        = $faker->name;
            $table->nit           = $faker->unique()->ipv4;
            $table->dui           = $faker->unique()->ipv4;
            $table->registro      = $faker->unique()->ipv4;
            $table->municipio     = $faker->city;
            $table->departamento  = $faker->country;
            $table->giro          = $faker->name;
            $table->direccion     = $faker->address;
            $table->telefono      = $faker->phoneNumber;
            $table->correo        = $faker->email;
            $table->empresa_id    = 1;
            $table->save();

            $table->save();
        }
        
    }
     
}
            
