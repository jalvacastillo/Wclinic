<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Traslado</title>
</head>


<style>
    .text-center { text-align: center; }
    .text-right { text-align: right; }
    *{ font-family: sans-serif; color: #333; }
    @page { margin: 70px 100px; }
    .header { position: fixed; top: -80px; opacity: .6;  }
    .footer{ position: fixed; bottom: -10px; opacity: .6; }
    .bg{ width: 840px; position: fixed; top: -150px; left: -120px; opacity: .5; z-index: -1;}
    table {width: 100%; border-collapse: collapse; margin: auto; page-break-inside: auto; }
    tr{ page-break-inside:avoid; page-break-after:auto }
    td, th, td, {border: 0.5px solid gray; padding: 5px 10px; }
    hr{ border: 0.5px solid #D1D0D0; }
    .notas>br:before {content: "*"; color: black; }
    p{ text-align: justify; }
    .badge{ background-color: #1B5FFA; color: white; padding: 5px;}
    .completado{text-decoration:line-through; color: gray;}
    .text-uppercase{text-transform: uppercase;}

</style>

<body>
	<h1 class="text-center">{{ $empresa->nombre }}</h1>
	<h2 class="text-center">Requisición # {{ $traslado->id }}</h2>
	<p><b>Fecha:</b> {{ \Carbon\Carbon::parse($traslado->fecha)->format('d-m-Y') }} 
		<b>Hora:</b> {{ \Carbon\Carbon::parse($traslado->fecha)->format('h:m:s A') }}</p>
	<p><b>De:</b> {{ $traslado->origen->nombre }} <b>Para:</b> {{ $traslado->destino->nombre }}</p>
	<p><b>Realizada por:</b> {{ $traslado->usuario }}</p>
	<p><b>Nota:</b> {{ $traslado->nota }}</p>
	<br>
	<table>
		<thead>
			<tr>
				<th>N°</th>
				<th>Producto</th>
				<th>Categoria</th>
				<th class="text-center">Medida</th>
				<th class="text-center">Cantidad</th>
			</tr>
		</thead>
		<tbody>
			@foreach($traslado->detalles as $key => $detalle)
			<tr>
				<td>{{ $key + 1}}</td>
				<td>{{ $detalle->producto_nombre }}</td>
				<td>{{ $detalle->producto()->first()->categoria }}</td>
				<td class="text-center">{{ $detalle->medida }}</td>
				<td class="text-center">{{ $detalle->cantidad }}</td>
			</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td class="text-right" colspan="4"><b>Total:</b></td>
				<td class="text-center"><b>{{ number_format($traslado->detalles->sum('cantidad')) }}</b></td>
			</tr>
		</tfoot>
	</table>

</body>
</html>