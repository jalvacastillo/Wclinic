<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Receta de Medicamentos</title>
</head>

<style>
    .text-center { text-align: center; }
    .text-right { text-align: right; }
    .text-left { text-align: left; }
    *{ font-family: sans-serif; color: #333; }
    @page { margin: 100px 100px; }
    .header { position: fixed; top: -100px; opacity: .6;  }
    .footer{ position: fixed; bottom: -10px; opacity: .6; }
    .bg{ width: 840px; position: fixed; top: -150px; left: -120px; opacity: .5; z-index: -1;}
    .table {width: 100%; border-collapse: collapse; margin: auto;}
    .table-bordered td, .table-bordered th, .table-bordered td, {border: 0.5px solid gray; padding: 5px 10px; }
    hr{ border: 0.5px solid #D1D0D0; }
    .notas>br:before {content: "*"; color: black; }
    p{ text-align: justify; }
    .badge{ background-color: #1B5FFA; color: white; padding: 5px;}
    .completado{text-decoration:line-through; color: gray;}

</style>

<body>
    {{-- <img class="bg" src="imgs/bg.jpg"> --}}
    <div class="header text-center">
        <img src="img/{{$consulta->sucursal->empresa->logo }}" alt="Logo" width="100">
    </div>
    <div class="footer">
        <hr>
        <h4 class="text-center">
            {{ $consulta->sucursal->empresa->nombre }} | {{ $consulta->sucursal->empresa->correo }} | {{ $consulta->sucursal->empresa->telefono }}
        </h4>
    </div>

    <h2 class="text-center">RECETA DE MEDICAMENTOS</h2>

    <p class="text-right">
        <b>{{ $consulta->sucursal->empresa->nombre }}</b><br>
        {{ $consulta->sucursal->empresa->propietario }}<br>
        {{ substr($consulta->fecha,0,10 ) }}
    </p>

    <table class="table">
        <tbody>
            <tr>
                <td>
                    <p class="text-left">
                        <b>Paciente:</b> <br>
                        <b>Nombre</b>: {{ $consulta->paciente()->first()->nombre }}<br>
                        <b>Edad</b>: {{ $consulta->paciente()->first()->edad }}<br>
                        <b>Diagnostico</b>: {{ $consulta->diagnosticos()->first()->descripcion }}
                    </p>
                </td>
                <td>
                    <p class="text-left">
                        <b>Receta:</b> <br>
                        <b>N°</b>: {{ $consulta->id }}<br>
                        <b>Lugar</b>: {{ $consulta->sucursal()->first()->municipio }}, {{ $consulta->sucursal()->first()->departamento }}<br>
                        <b>Fecha</b>: {{ $consulta->fecha }}
                    </p>
                </td>
            </tr>
        </tbody>
        
    </table>

    <table class="table table-bordered"  style="margin-top: 20px;">
        <thead>
            <tr>
                <th class="text-center">Cantidad</th>
                <th>Medicamento</th>
                <th>Indicaciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($consulta->recetas as $producto)
                <tr>
                    <td class="text-center">{{ $producto->cantidad }}</td>
                    <td>{{ $producto->nombre_producto }}</td>
                    <td>{{ $producto->dosis }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <p class="text-left" style="margin-top: 20px;">
        <b>Instrucciones:</b>
        <br>
        {{ $consulta->receta }} <br>
    </p>

    <p class="text-center" style="margin-top: 200px;">
        _________________________________________
        <br>
        <b>Firma del médico:</b>
    </p>

    

</body>
</html>
