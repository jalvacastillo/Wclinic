<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-4">
            <h2 class="my-0 title text-center text-md-left"> Ventajas de tener un software </h2>
            <h5 class="description text-justify">
                Un sistema para gasolineras supone un gran apoyo en la gestión diaria de una estación de servicio y aporta las siguientes ventajas:
            </h5>
        </div>
        <div class="col-12 col-md-6 text-justify">
            <ol>
                <li>
                    <b>Mayor control:</b> 
                    Podrás controlar y monitorizar tu negocio desde cualquier lugar.
                </li>
                <li>
                    <b>Más rapidez:</b> 
                    Serás capaz de agilizar los procesos administrativos y la gestión financiera del negocio.
                </li>
                <li>
                    <b>Optimización de recursos:</b> 
                    Conseguirás optimizar la gestión del personal y de recursos en tu estación de servicio.
                </li>
                <li>
                    <b>Automatización:</b> 
                    Reducirás las operaciones manuales para poder invertir más tiempo en tareas que aporten más valor.
                </li>
                <li>
                    <b>Disminución de errores:</b> 
                    Lograrás minimizar la aparición de errores en las gestiones y procesos claves.
                </li>
                <li>
                    <b>Óptima atención al cliente:</b> 
                    Lograrás agilizar los servicios ofrecidos y simplificar el proceso de venta, para brindar una mejor atención al cliente.
                </li>
                <li>
                    <b>Completo control del stock:</b> 
                    Podrás llevar a cabo un control del inventario de productos y asegurar la disponibilidad de carburante manera sencilla.
                </li>
                <li>
                    <b>Mejorar la toma de decisiones:</b> 
                    Podrás ver en tiempo real todos los movimientos y resultados de tu estación de servicio, lo que ayudará a tomar mejores decisiones.
                </li>
            </ol>
        </div>
    </div>

</div>