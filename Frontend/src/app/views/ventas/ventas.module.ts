import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FocusModule } from 'angular2-focus';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { VentasRoutingModule } from './ventas.routing.module';

import { CreditosModule } from '../creditos/creditos.module';

import { VentasComponent } from './ventas.component';
import { VentaComponent } from './venta/venta.component';
import { DevolucionesVentasComponent } from './devoluciones/devoluciones-ventas.component';
import { DevolucionVentaComponent } from './devoluciones/devolucion/devolucion-venta.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    PipesModule,
    CreditosModule,
    VentasRoutingModule,
    PopoverModule.forRoot(),
    FocusModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
  ],
  declarations: [
    VentasComponent,
    VentaComponent,
    DevolucionesVentasComponent,
    DevolucionVentaComponent
  ],
  exports: [
    VentasComponent,
    VentaComponent,
    DevolucionesVentasComponent,
    DevolucionVentaComponent
  ]
})
export class VentasModule { }
