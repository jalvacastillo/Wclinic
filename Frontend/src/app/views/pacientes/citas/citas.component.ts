import { Component, OnInit, TemplateRef } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { CalendarOptions } from '@fullcalendar/core'; // useful for typechecking
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import esLocale from '@fullcalendar/core/locales/es';

import { ApiService } from '../../../services/api.service';
import { AlertService } from '../../../services/alert.service';

@Component({
  selector: 'app-citas',
  templateUrl: './citas.component.html',
})
export class CitasComponent implements OnInit {

    public citas:any;
    public buscador:any = '';
    public loading:boolean = false;

    public filtro:any = {};
    public filtrado:boolean = false;
    public usuarios:any = [];


    calendarOptions: CalendarOptions = {
        initialView: 'dayGridMonth',
        plugins: [dayGridPlugin, timeGridPlugin],
        locale: esLocale,
        headerToolbar: {
            right: "prev,next",
            center: "title",
            left: "dayGridMonth,timeGridWeek,timeGridDay"
        },
        eventDisplay: 'block',
        allDaySlot: false,
        themeSystem: 'bootstrap5',
        events: [{
            id : '1',
            title : 'Hola',
            start : '2023-02-02',
            end : '2023-02-02',
            type : 'Uno',
            color: '#367837',
        }]
      };
    
    modalRef?: BsModalRef;

    constructor( public apiService:ApiService, private alertService:AlertService, private modalService: BsModalService ){}

    ngOnInit() {
        this.loadAll();
    }

    public loadAll() {
        this.loading = true;
        this.apiService.getAll('citas').subscribe(citas => { 
            this.citas = citas;
            // this.calendarOptions.events = this.citas.data;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }

    public search(){
        if(this.buscador && this.buscador.length > 2) {
            this.loading = true;
            this.apiService.read('citas/buscar/', this.buscador).subscribe(citas => { 
                this.citas = citas;
                this.loading = false; this.filtrado = true;
            }, error => {this.alertService.error(error); this.loading = false;});
        }else{
            this.loadAll();
        }
    }

    public delete(consulta:any){
        if (confirm('¿Desea eliminar el Registro?')) {
            this.apiService.delete('consulta/', consulta.id) .subscribe(data => {
                for (let i = 0; i < this.citas.data.length; i++) { 
                    if (this.citas.data[i].id == data.id )
                        this.citas.data.splice(i, 1);
                }
            }, error => {this.alertService.error(error); });
                   
        }
    }

    public setEstado(consulta:any, estado:any):void{
        this.loading = true;
        consulta.estado = estado;
        this.apiService.store('consulta', consulta).subscribe(data => {
            this.loading = false;
            consulta = data;
            this.alertService.success('Guardado');
        },error => {this.alertService.error(error); this.loading = false; });
    }

    public setPagination(event:any):void{
        this.loading = true;
        this.apiService.paginate(this.citas.path + '?page='+ event.page).subscribe(citas => { 
            this.citas = citas;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


    // Filtros
    openFilter(template: TemplateRef<any>) {     

        if(!this.filtrado) {
            this.filtro.inicio = null;
            this.filtro.fin = null;
            this.filtro.usuario_id = '';
            this.filtro.estado = '';
            this.filtro.metodo_pago = '';
            this.filtro.tipo_documento = '';
        }
        if(!this.usuarios.data){
            this.apiService.getAll('usuarios/filtrar/tipo/Mesero').subscribe(usuarios => { 
                this.usuarios = usuarios.data;
            }, error => {this.alertService.error(error); });
        }
        this.modalRef = this.modalService.show(template);
    }

    onFiltrar(){
        this.loading = true;
        this.apiService.store('citas/filtrar', this.filtro).subscribe(citas => { 
            this.citas = citas;
            this.loading = false; this.filtrado = true;
            this.modalRef!.hide();
        }, error => {this.alertService.error(error); this.loading = false;});

    }


}
