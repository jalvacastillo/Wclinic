import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgChartsModule } from 'ng2-charts';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';


@Component({
  selector: 'app-dash-ordenes',
  templateUrl: './dash-ordenes.component.html'
})
export class DashOrdenesComponent implements OnInit {

   @Input() dash:any = {};
   public loading:boolean = false;

	constructor( private alertService:AlertService, private apiService:ApiService
	) { }

	ngOnInit() {

  }

}
