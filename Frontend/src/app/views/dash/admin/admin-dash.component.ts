import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../../services/alert.service';
import { ApiService } from '../../../services/api.service';

declare var $: any;

@Component({
  selector: 'app-admin-dash',
  templateUrl: './admin-dash.component.html'
})
export class AdminDashComponent implements OnInit {

    public dash:any = {};
    public sucursales:any[] = [];
    public filtro:any = {};
    public loading:boolean = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService
    ) { }


    ngOnInit() {

        this.filtro.inicio  = this.apiService.date();
        this.filtro.fin     = this.apiService.date();
        this.filtro.sucursal_id = '';

        $('#top_menu_carousel').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            responsive:{
                0:{
                    items:1
                },
                600:{
                    items:4
                },
                1000:{
                    items:4
                }
            }
        })

        this.loadAll();

        this.apiService.getAll('sucursales').subscribe(sucursales => { 
            this.sucursales = sucursales;
        }, error => {this.alertService.error(error); });


    }
    
    public loadAll(){
        this.loading = true;
        this.apiService.store('dash', this.filtro).subscribe(dash => {
            this.dash = dash;
            this.loading = false;
        }, error => {this.alertService.error(error); this.loading = false;});
    }


}
