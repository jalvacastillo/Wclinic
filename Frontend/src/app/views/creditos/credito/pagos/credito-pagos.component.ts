import { Component, OnInit, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../services/api.service';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-credito-pagos',
  templateUrl: './credito-pagos.component.html'
})
export class CreditoPagosComponent implements OnInit {

    @Input() credito: any = {};
    public pago:any = {};
    public cantidad!:number;

    @Output() update = new EventEmitter();
    modalRef?: BsModalRef;

    public buscador:string = '';
    public loading:boolean = false;

    constructor( 
        public apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {

    }

    openModal(template: TemplateRef<any>, pago:any) {
        this.pago = pago;

        if (!this.pago.id) {
            this.pago.fecha = this.apiService.date();
            this.pago.cuota = this.credito.cuota;
            this.pago.credito_id = this.credito.id;
            this.pago.metodo_pago = 'Efectivo';
            this.pago.usuario_id = this.apiService.auth_user().id;
            this.onCalcular();
            this.pago.cuota = this.credito.cuota.toFixed(2);
            this.pago.saldo_inicial = this.credito.saldo;
            this.pago.saldo_inicial = this.credito.saldo;
        }
        this.modalRef = this.modalService.show(template, {class: 'modal-md'});
    }

    onCalcular(){
        this.pago.interes_mensual = (this.credito.interes / 12 )/ 100;
        this.pago.interes = this.credito.saldo * this.pago.interes_mensual;
        this.pago.abono = this.pago.cuota - this.pago.interes;
        this.pago.saldo_final = this.credito.saldo - this.pago.abono;

        console.log(this.pago);
    }

    public onSubmit() {

        this.loading = true;

        this.apiService.store('credito/pago', this.pago).subscribe(credito => {
            this.loading = false;
            this.credito.pagos.push(this.pago);
            this.update.emit(this.credito);
            this.pago = {};
            this.modalRef!.hide();
            this.alertService.success("Guardado");
        },error => {this.alertService.error(error); this.loading = false; });

    }


    // Eliminar pago
        public eliminarDetalle(pago:any){
            if (confirm('Confirma que desea eliminar el elemento')) { 
                this.apiService.delete('credito/pago/', pago.id).subscribe(pago => {
                    for (var i = 0; i < this.credito.pagos.length; ++i) {
                        if (this.credito.pagos[i].id === pago.id ){
                            this.credito.pagos.splice(i, 1);
                            this.update.emit(this.credito);
                        }
                    }
                },error => {this.alertService.error(error); this.loading = false; });
            }

        }


}
