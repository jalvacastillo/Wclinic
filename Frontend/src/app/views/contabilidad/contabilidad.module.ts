import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { FocusModule } from 'angular2-focus';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { NgChartsModule } from 'ng2-charts';

import { ContabilidadRoutingModule } from './contabilidad.routing.module';

import { LibroIvaComponent } from './libro-iva/libro-iva.component';
import { LibroComprasComponent } from './libro-compras/libro-compras.component';
import { GalonajeComponent } from './galonaje/galonaje.component';
import { GastosComponent } from './gastos/gastos.component';
import { GastosDashComponent } from './gastos/dash/gastos-dash.component';
import { CajasChicasComponent } from './cajas-chicas/cajas-chicas.component';
import { CajaChicaComponent } from './cajas-chicas/caja-chica/caja-chica.component';
import { CajaChicaDetallesComponent } from './cajas-chicas/caja-chica/detalles/caja-chica-detalles.component';
import { ActivosComponent } from './activos/activos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    SharedModule,
    NgChartsModule,
    ContabilidadRoutingModule,
    FocusModule.forRoot(),
    TooltipModule.forRoot()
  ],
  declarations: [
    LibroIvaComponent,
  	LibroComprasComponent,
    GalonajeComponent,
    GastosComponent,
    GastosDashComponent,
    CajasChicasComponent,
    CajaChicaComponent,
    CajaChicaDetallesComponent,
    ActivosComponent
  ],
  exports: [
    LibroIvaComponent,
  	LibroComprasComponent,
    GalonajeComponent,
    GastosComponent,
    GastosDashComponent,
    CajasChicasComponent,
    CajaChicaComponent,
    CajaChicaDetallesComponent,
    ActivosComponent
  ]
})
export class ContabilidadModule { }
