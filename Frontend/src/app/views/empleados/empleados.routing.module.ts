import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '../../layout/layout.component';

import { EmpleadosComponent }     from './empleados.component';
import { EmpleadoComponent }     from './empleado/empleado.component';
import { AsistenciasComponent }     from './asistencias/asistencias.component';
import { AsistenciaComponent }     from './asistencias/asistencia/asistencia.component';
import { PlanillasComponent }     from './planillas/planillas.component';
import { PlanillaComponent }     from './planillas/planilla/planilla.component';
import { ComisionesComponent }     from './comisiones/comisiones.component';
import { PropinasComponent }     from '../../views/empleados/propinas/propinas.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
        { path: 'empleados', component: EmpleadosComponent },
        { path: 'empleado/:id', component: EmpleadoComponent },
        { path: 'asistencias', component: AsistenciasComponent },
        { path: 'asistencia', component: AsistenciaComponent },
        { path: 'planillas', component: PlanillasComponent },
        { path: 'planilla/:id', component: PlanillaComponent },
        { path: 'comisiones', component: ComisionesComponent },
        { path: 'propinas', component: PropinasComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
