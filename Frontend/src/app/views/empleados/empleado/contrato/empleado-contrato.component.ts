import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-empleado-contrato',
  templateUrl: './empleado-contrato.component.html'
})
export class EmpleadoContratoComponent implements OnInit {

    @Input() empleado: any = {};
    public loading = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router
    ) { }

    ngOnInit() {
    }


    public onSubmit() {
        this.loading = true;
        this.empleado.contrato.empleado_id = this.empleado.id;
        this.apiService.store('contrato', this.empleado.contrato).subscribe(contrato => {
            this.empleado.contrato = contrato;
            this.alertService.success("Guardado");
            this.loading = false;
        },error => {this.alertService.error(error); this.loading = false; });

    }

}
