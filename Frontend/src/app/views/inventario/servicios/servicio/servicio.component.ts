import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { AlertService } from '../../../../services/alert.service';
import { ApiService } from '../../../../services/api.service';

@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html'
})
export class ServicioComponent implements OnInit {

    public producto: any = {};
    public categorias:any[] = [];
    public loading = false;

    constructor( 
        private apiService: ApiService, private alertService: AlertService,
        private route: ActivatedRoute, private router: Router,
    ) {
        this.router.routeReuseStrategy.shouldReuseRoute = function() {return false; };
    }

    ngOnInit() {

        this.apiService.getAll('categorias').subscribe(categorias => {
            this.categorias = categorias;
        }, error => {this.alertService.error(error);});
        
        this.loadAll();

    }

    loadAll(){
        const id = +this.route.snapshot.paramMap.get('id')!;
            
        if(isNaN(id)){
            this.producto = {};
            this.producto.tipo = 'Servicio';
            this.producto.empresa_id = this.apiService.auth_user().empresa_id;
        }
        else{
            // Optenemos el servicio
            this.loading = true;
            this.apiService.read('servicio/', id).subscribe(servicio => {
               this.producto = servicio;
               this.loading = false;
            },error => {this.alertService.error(error);this.loading = false;});
        }

    }

    public onSubmit() {
        this.loading = true;
        this.apiService.store('servicio', this.producto).subscribe(servicio => {
            this.loading = false;
            if(!this.producto.id) {
                this.producto = servicio;
                this.router.navigate(['/servicio/'+ servicio.id]);
            }
            this.alertService.success("Producto guardado");
        },error => {this.alertService.error(error); this.loading = false; });
    }
    

}
