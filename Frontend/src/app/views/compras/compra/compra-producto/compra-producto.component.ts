import { Component, OnInit, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { fromEvent, timer } from 'rxjs';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../services/api.service';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-compra-producto',
  templateUrl: './compra-producto.component.html'
})
export class CompraProductoComponent implements OnInit {

	@Output() productoSelect = new EventEmitter();
	modalRef!: BsModalRef;

	public productos:any = [];
    public producto: any = {};
	public detalle: any = {};
    public searching:boolean = false;


	constructor( 
	    private apiService: ApiService, private alertService: AlertService,
	    private modalService: BsModalService
	) { }

	ngOnInit() {
	}

	openModal(template: TemplateRef<any>) {
        this.detalle = {};
        this.producto = {};

        this.detalle.tipo = 'Gravada';
        this.detalle.descuento = 0;
        // this.detalle.otros = 0;

        this.modalRef = this.modalService.show(template, {class: 'modal-lg', backdrop: 'static'});

        const input = document.getElementById('example')!;
        const example = fromEvent(input, 'keyup').pipe(map(i => (<HTMLTextAreaElement>i.currentTarget).value));
        const debouncedInput = example.pipe(debounceTime(500));
        const subscribe = debouncedInput.subscribe(val => { this.searchProducto(); });
    }

	searchProducto(){
            if(this.producto.nombre && this.producto.nombre.length > 1) {
            this.searching = true;
            this.apiService.read('productos-all/buscar/', this.producto.nombre).subscribe(productos => {
               if(productos.total == 1) {
                   this.selectProducto(productos.data[0]);
                   this.searching = false;
               }else{
                   this.productos = productos;
                   this.searching = false;
               }
            }, error => {this.alertService.error(error);this.searching = false;});
        }else if (!this.producto.nombre  || this.producto.nombre.length < 1 ){ this.searching = false; this.producto = {}; this.productos.total = 0; }
    }

    selectProducto(producto:any){
        this.producto = producto;
    	this.detalle.producto_id     = producto.id;
        this.detalle.categoria_id    = producto.categoria_id;
        this.detalle.nombre_producto = producto.nombre;
        this.detalle.precio          = producto.precio;
        this.detalle.precio_nuevo    = producto.precio;
        this.detalle.margen          = 35;
            // this.detalle.precio_nuevo    = (parseFloat(this.detalle.costo_iva) + (this.detalle.costo_iva * (this.detalle.margen / 100))).toFixed(2);
        this.detalle.medida          = producto.medida;
        this.detalle.inventarios     = producto.inventarios;
        if (this.detalle.inventarios.length > 0) {
            this.detalle.inventario_id    = this.detalle.inventarios[0].id;
        }

    	this.productos.total = 0;
    	document.getElementById('cantidad')!.focus();
    	this.producto.cantidad = 1;
    }

    calcularByCosto(){

        if(this.detalle.costo) {
            this.detalle.costo_iva  = (parseFloat(this.detalle.costo) + (this.detalle.costo * 0.13)).toFixed(2);
        	if(!this.detalle.subtotal){
                this.detalle.subtotal = ((this.detalle.cantidad * this.detalle.costo) - this.detalle.descuento).toFixed(2);
            }
        }
        
        this.detalle.iva = 0;
        if(this.detalle.tipo == 'Gravada'){
            this.detalle.iva = (this.detalle.subtotal * 0.13).toFixed(2);
        }
        
        // this.detalle.fovial = 0;
        // this.detalle.cotrans = 0;
        this.detalle.total = (parseFloat(this.detalle.subtotal) + parseFloat(this.detalle.iva)).toFixed(2);
  
    }
    
    calcularBySubtotal(){
        this.detalle.costo = (this.detalle.subtotal / this.detalle.cantidad).toFixed(4);
        this.calcularByCosto();
    }
    
    calcularByTotal(){
        this.detalle.subtotal = (this.detalle.total / 1.13).toFixed(2);
        this.detalle.costo = (this.detalle.subtotal / this.detalle.cantidad).toFixed(4);
        this.detalle.costo_iva  = (parseFloat(this.detalle.costo) + (this.detalle.costo * 0.13)).toFixed(2);
        this.detalle.iva = 0;
        if(this.detalle.tipo == 'Gravada'){
            this.detalle.iva = (this.detalle.subtotal * 0.13).toFixed(2);
        }
        // this.calcularByCosto();
    }

    agregarDetalle(){
        this.productoSelect.emit({detalle: this.detalle});
        this.modalRef.hide();
        this.clear();
	}

    clear(){
        if(this.productos.data && this.productos.data.length == 0) { this.productos = []; }
    }

}
