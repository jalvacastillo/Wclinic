import { Component, OnInit, EventEmitter, Input, Output, TemplateRef, ViewChild } from '@angular/core';
import { BsModalService, BsModalRef} from 'ngx-bootstrap/modal';

import { ApiService } from '../../../../services/api.service';
import { AlertService } from '../../../../services/alert.service';

@Component({
  selector: 'app-compra-detalles',
  templateUrl: './compra-detalles.component.html'
})
export class CompraDetallesComponent implements OnInit {

    @Input() compra: any = {};
    public detalle: any = {};
    public detalleModificado: any = {};
    public cantidad!:number;

    @Output() update = new EventEmitter();
    @Output() sumTotal = new EventEmitter();
    modalRef?: BsModalRef;

    public buscador:string = '';
    public loading:boolean = false;

    constructor( 
        public apiService: ApiService, private alertService: AlertService,
        private modalService: BsModalService
    ) { }

    ngOnInit() {

    }

    public editarDetalle(template: TemplateRef<any>, detalle:any) {
        this.detalle = detalle;
        this.detalleModificado.producto_id = this.detalle.producto_id;
        this.detalleModificado.tipo = this.detalle.tipo;
        this.modalRef = this.modalService.show(template, {class: 'modal-lg', backdrop: 'static'});
    }

    public guardarDetalle() {
        this.detalle.cantidad = this.detalleModificado.cantidad;
        this.detalle.costo = this.detalleModificado.costo;
        this.detalle.costo_iva = this.detalleModificado.costo_iva;
        this.detalle.cotrans = this.detalleModificado.cotrans;
        this.detalle.fovial = this.detalleModificado.fovial;
        this.detalle.iva = this.detalleModificado.iva;
        this.detalle.tipo = this.detalleModificado.tipo;
        this.detalle.total = this.detalleModificado.total;
        this.detalle.subtotal = this.detalleModificado.subtotal;
        this.modalRef?.hide();
        this.update.emit(this.compra);
    }

    calcular(){

        if(this.detalleModificado.costo) {
            this.detalleModificado.costo_iva  = (parseFloat(this.detalleModificado.costo) + (this.detalleModificado.costo * 0.13)).toFixed(2);
            if(!this.detalleModificado.subtotal){
                this.detalleModificado.subtotal = ((this.detalleModificado.cantidad * this.detalleModificado.costo) - this.detalleModificado.descuento).toFixed(2);
            }
        }
        
        this.detalleModificado.iva = 0;
        if(this.detalleModificado.tipo == 'Gravada'){
            this.detalleModificado.iva = (this.detalleModificado.subtotal * 0.13).toFixed(2);
        }
        
        this.detalleModificado.fovial = 0;
        this.detalleModificado.cotrans = 0;
        this.detalleModificado.total = (parseFloat(this.detalleModificado.subtotal) + parseFloat(this.detalleModificado.iva) + parseFloat(this.detalleModificado.fovial) + parseFloat(this.detalleModificado.cotrans)).toFixed(2);
  
    }
    
    calcularCosto(){
        this.detalleModificado.costo = (this.detalleModificado.subtotal / this.detalleModificado.cantidad).toFixed(4);
        this.calcular();
    }


    // Eliminar detalle
        public eliminarDetalle(detalle:any){
            if (confirm('Confirma que desea eliminar el elemento')) { 
                if(detalle.id) {
                    this.apiService.delete('compra/detalle/', detalle.id).subscribe(detalle => {
                        for (var i = 0; i < this.compra.detalles.length; ++i) {
                            if (this.compra.detalles[i].id === detalle.id ){
                                this.compra.detalles.splice(i, 1);
                                this.update.emit(this.compra);
                            }
                        }
                    },error => {this.alertService.error(error); this.loading = false; });
                }else{

                    for (var i = 0; i < this.compra.detalles.length; ++i) {
                        if (this.compra.detalles[i].producto_id === detalle.producto_id ){
                            this.compra.detalles.splice(i, 1);
                            this.update.emit(this.compra);
                        }
                    }
                }
            }

        }

    public sumTotalEmit(){
        this.sumTotal.emit();
    }

}
