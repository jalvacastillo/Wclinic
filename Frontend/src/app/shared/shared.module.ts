import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FocusModule } from 'angular2-focus';

import { MultimediaComponent } from './multimedia/multimedia.component';
import { PipesModule } from './../pipes/pipes.module';

import { BusquedaClienteComponent } from './modals/busqueda-cliente/busqueda-cliente.component';
import { BusquedaProductoComponent } from './modals/busqueda-producto/busqueda-producto.component';
import { ClienteDireccionComponent } from './modals/cliente-direccion/cliente-direccion.component';

import { BuscadorProductosComponent } from './parts/buscador-productos/buscador-productos.component';
import { BuscadorClientesComponent } from './parts/buscador-clientes/buscador-clientes.component';
import { BuscadorMateriasPrimasComponent } from './parts/buscador-materias-primas/buscador-materias-primas.component';

import { PaginationComponent } from './parts/pagination/pagination.component';
import { TimerComponent } from './parts/timer/timer.component';
import { NotFoundComponent } from './404/not-found.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PipesModule,
    FocusModule.forRoot()
  ],
  declarations: [
    BusquedaClienteComponent,
    BusquedaProductoComponent,
    ClienteDireccionComponent,
    MultimediaComponent,
    BuscadorProductosComponent,
    BuscadorClientesComponent,
    BuscadorMateriasPrimasComponent,
    PaginationComponent,
    TimerComponent,
    NotFoundComponent
  ],
  exports: [
    BusquedaClienteComponent,
    BusquedaProductoComponent,
    ClienteDireccionComponent,
    MultimediaComponent,
    BuscadorProductosComponent,
    BuscadorClientesComponent,
    BuscadorMateriasPrimasComponent,
    PaginationComponent,
    TimerComponent,
    NotFoundComponent
  ]
})
export class SharedModule { }
